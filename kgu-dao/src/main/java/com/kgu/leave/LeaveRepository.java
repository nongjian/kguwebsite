package com.kgu.leave;

import com.kgu.entity.leave.Leave;
import com.kgu.entity.link.LinkInfo;
import com.kgu.entity.section.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LeaveRepository extends JpaRepository<Leave, String>, JpaSpecificationExecutor<Leave> {
}
