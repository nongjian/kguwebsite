package com.kgu.link;

import com.kgu.entity.link.LinkInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface LinkRepository extends JpaRepository<LinkInfo, String>,JpaSpecificationExecutor<LinkInfo> {
}
