package com.kgu.tag;

import com.kgu.entity.tag.Tag;
import com.kgu.enums.SysLanguage;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 16:07
 **/
public interface TagRepository extends JpaRepository<Tag,String>, JpaSpecificationExecutor<Tag> {
    Optional<Tag> findByIdAndSiteIdAndDeletedIsFalse(String id, String siteId);

    Optional<Tag> findByIdAndDeletedIsFalse(String id);

    List<Tag> findByNameLikeAndDeletedIsFalse(String name);

    List<Tag> findBySectionIdAndDeletedIsFalse(String sectionId);

    List<Tag>findAllByDeletedIsFalse();

    List<Tag> findBySectionIdAndDeletedIsFalseAndAvailableIsTrue(String id);

    List<Tag> findBySectionIdAndSysLanguageAndDeletedIsFalseAndAvailableIsTrue(String id, SysLanguage sysLanguage);

    List<Tag> findAllBySectionIdNotNullAndDeletedIsFalseAndAvailableIsTrue();
}
