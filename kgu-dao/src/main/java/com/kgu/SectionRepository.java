package com.kgu;

import com.kgu.entity.section.Section;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
//1.475
public interface SectionRepository extends JpaRepository<Section,String> {
    @Cacheable(value = "kguSection")
    List<Section> findAllByParentIdIsNullAndAvailableIsTrue();

    List<Section> findAllByAvailableIsTrueAndDeletedIsFalse();

    Section findOneByName(String name);

    Section findOneByTemplate(String name);
    @Query(value = "select * from c_section as cs where cs.title like CONCAT('%',?1,'%')",nativeQuery = true)
    Section findByTitleAndDeletedIsFalseAndAvailableIsTrue(String title);

    List<Section> findAllByParentIdAndAvailableIsTrue(String parentId);

    Section findAllByTemplate(String template);

    Section  findOneById(String id);


}
