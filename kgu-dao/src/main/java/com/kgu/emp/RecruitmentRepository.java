package com.kgu.emp;


import com.kgu.entity.emp.Recruitment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RecruitmentRepository  extends JpaRepository<Recruitment,String>, JpaSpecificationExecutor<Recruitment> {

    @Query(value = "SELECT dd.* FROM emp_recruitment dd where deleted = 0 and available = 1  group by work_city" ,nativeQuery = true)
    List<Recruitment> getWorkCityList();

    @Query(value = "SELECT dd.*  FROM emp_recruitment dd where work_city = ?1  and deleted = 0 and available = 1   group by position_name" ,nativeQuery = true)
    List<Recruitment> getPositionNameList(String work_city);

    List<Recruitment> findAllByWorkCity(String  workCity);

    List<Recruitment> findAllByPositionName(String PositionName);

    List<Recruitment> findAllBySiteIdAndPositionNameAndDeletedIsFalse(String  siteId,String PositionName);

    List<Recruitment> findAllByWorkCityAndPositionNameAndDeletedIsFalse(String  siteId,String PositionName);

    Recruitment findOneById(String id);

    Recruitment findByWorkCity(String workCity);


    List<Recruitment> findAllByWorkCityAndDeletedIsFalseAndAvailableIsTrue(String workCity);


}
