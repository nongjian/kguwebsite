package com.kgu.data;

import com.kgu.entity.data.DataAttribute;
import com.kgu.request.DataAttributeDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ModelAttributeRepository extends JpaRepository<DataAttribute,String>, JpaSpecificationExecutor<DataAttribute> {

    @Query(value = "select new com.kgu.request.DataAttributeDTO(cda,ca.name) from " +
                    " c_attribute as ca " +
                    "join c_data_attribute as cda " +
                    "on ca.id = cda.attributeId " +
                    "where " +
                    "cda.targetId =:targetId and " +
                    "ca.modelId = :modelId and " +
                    "cda.value != ''",nativeQuery = false)
    List<DataAttributeDTO> findModelAttribute(@Param("targetId") String targetId, @Param("modelId") String modelId);
}
