package com.kgu.data;

import com.kgu.entity.basic.BasicInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 14:44
 **/
public interface BasicInfoRepository extends JpaRepository<BasicInfo,String>, JpaSpecificationExecutor<BasicInfo> {

    Optional<BasicInfo> findFirstBySiteIdAndDeletedIsFalseAndAvailableIsTrue(String currentSiteId);

    BasicInfo findOneBySiteIdAndDeletedIsFalseAndAvailableIsTrue(String currentSiteId);



}
