package com.kgu.data;

import com.kgu.entity.basic.BasicInfo;
import com.kgu.entity.basic.BasicRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface BasicRuleRepository extends JpaRepository<BasicRule,String>, JpaSpecificationExecutor<BasicRule> {


   List<BasicRule> findAllByDeletedIsFalseAndAvailableIsTrue();
   @Query(value = "select * from c_kgu_basic_rule as cd where cd.deleted= 0 and cd.available=1 and cd.position_name='PRODUCT' order by cd.comparable ASC",nativeQuery = true)
   List<BasicRule>  getAll();



}
