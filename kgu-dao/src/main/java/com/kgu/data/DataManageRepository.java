package com.kgu.data;

import com.kgu.entity.data.DataManage;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface DataManageRepository extends JpaRepository<DataManage,String>, JpaSpecificationExecutor<DataManage> {



    Optional<DataManage> findBySeoname(String name);

    DataManage findAllByIdAndDeletedIsFalse(String id);

    List<DataManage> findByDeletedIsFalseAndAvailableIsTrue();


    List<DataManage> findBySectionIdAndDeletedIsFalse(String sectionId);

    @Query(value = "select * from c_data as cd where cd.type like CONCAT('%',:sectionId,'%') and cd.deleted=0 and cd.available=1 order by cd.comparable",nativeQuery = true)
    List<DataManage> queryExpandSection(@Param("sectionId") String sectionId);

    @Query(value = "select * from c_data as cd where cd.section_id=:sectionId order by cd.count limit 0,5",nativeQuery = true)
    List<DataManage> hotNews(@Param("sectionId") String sectionId);

    List<DataManage> findAllBySectionIdAndAvailableIsTrueAndDeletedIsFalse(String sectionId);

    @Query(value = "SELECT dd.* FROM c_data dd WHERE dd.id = (SELECT max(d.id) FROM c_data d WHERE d.id < ?1)" ,nativeQuery = true)
    DataManage next(String id);

    @Query(value = "SELECT dd.*  FROM c_data dd WHERE dd.id = (SELECT min(d.id) FROM c_data d WHERE d.id >?1)" ,nativeQuery = true)
    DataManage last(String id);

    @Query(value = "select * from c_data as cd where cd.title = ?1 limit 0,1",nativeQuery = true)
    DataManage searchData(String title);


    @Query(value = "select * from c_data as cd where cd.section_id=:sectionId order by cd.comparable asc limit 0,:count",nativeQuery = true)
    List<DataManage> findAllBySectionIdAndLimit(@Param("sectionId") String sectionId, @Param("count")  Integer count);

    @Query(value = "select * from c_data as cd where cd.section_id=:sectionId or cd.type like CONCAT('%',:sectionId,'%') and cd.deleted=0 and cd.available=1  order by cd.comparable desc limit 0,:count",nativeQuery = true)
    List<DataManage> findAllBySectionIdAndLimit2(@Param("sectionId") String sectionId, @Param("count")  Integer count);

    @Query(value = "select * from c_data as cd where cd.section_id=:sectionId or cd.`type` like CONCAT('%',:sectionId,'%') order by cd.comparable asc limit 0,:count",nativeQuery = true)
    List<DataManage> findAllBySectionIdAndExpend(@Param("sectionId") String sectionId, @Param("count")  Integer count);



    /**
     * 热门标签 --->关键词
     */
    @Query(value = "select * from c_data as cd where cd.type like CONCAT('%',?1,'%')",nativeQuery = true)
    List<DataManage> hotTypeList(String keyWord);

    @Query(value = "SELECT " +
            "data0.comparable," +
            "data0.section_id," +
            "cd2.* " +
            "FROM (" +
            "SELECT " +
            "cd.*," +
            "cdl.data_id," +
            "cdl.link_id " +
            "FROM " +
            "kgu_java_test.c_data as cd " +
            "left join kgu_java_test.c_data_link as cdl on " +
            "cdl.data_id = cd.id) as data0 " +
            "left join kgu_java_test.c_data as cd2 on " +
            "cd2.id = data0.link_id " +
            "WHERE " +
            "cd2.id IS NOT NULL " +
            "AND data0.id = :Id "+
            "AND if(:title is not null and :title != '',cd2.title LIKE CONCAT('%',:title,'%'),1=1) "+
            "AND if(:type is not null and :type != '',cd2.type LIKE CONCAT('%',:type,'%'),1=1) "+
            "ORDER BY " +
            "data0.comparable," +
            "cd2.comparable",nativeQuery = true)
    List<DataManage> findCaseList(@Param("Id") String Id,
                                  @Param("title") String title,
                                  @Param("type") String type);

    @Query(value = "select * from c_data as cd where cd.section_id = :sectionId " +
            "OR if(:title is not null and :title != '',cd.title LIKE CONCAT('%',:title,'%'),0=0) "+
            "OR if(:type is not null and :type != '',cd.type LIKE CONCAT('%',:type,'%'),0=0)"
            ,nativeQuery = true)
    Page<DataManage> findCase(@Param("sectionId") String sectionId,
                              @Param("title") String title,
                              @Param("type") String type,
                              Pageable pageable);

    /**
     * 推荐新闻
     * @param sectionId
     * @return
     */
    @Query(value = "select * from c_data as cd where cd.recommend = 1 and cd.section_id=?1  limit 0,9",nativeQuery = true)
    List<DataManage> recommendList(String sectionId);

    /**
     * 获取对应的基本法页内容
     */
    @Query(value = "select * from c_data as cd where cd.title='基本法' and cd.section_id=?1 ",nativeQuery = true)
    DataManage getBasciLaw(String id);

    @Query(value = "select * from c_data as cs where cs.section_id=:sectionId and cs.deleted=0 and cs.available=1 order by cs.comparable desc limit 0,3",nativeQuery = true)
    List<DataManage> findNewest(@Param("sectionId") String sectionId);


}
