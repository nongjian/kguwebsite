package com.kgu.data;

import com.kgu.common.Result;
import com.kgu.entity.data.DataManage;
import com.kgu.request.QueryDataManagePageRequest;
import org.springframework.data.domain.Page;
import org.springframework.ui.Model;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

public interface IDataManageService {

    List<DataManage> findAllByav();
    /**
     * 分页查询数据管理
     * @param request
     * @return
     */
    Result<Page<DataManage>> search(QueryDataManagePageRequest request);


    List<DataManage> searchList(String sectionId);

    List<DataManage> queryExpandSection(String sectionId);


    List<DataManage> searchList(String sectionId,String query);


    List<DataManage> findBySectionIdAndLimit(String sectionId,Integer count);

    List<DataManage> findBySectionIdAndExpendSection(String sectionId,Integer count);


    Result<Page<DataManage>>  findAllBySectionIds(List<String> ids,QueryDataManagePageRequest request);

    Optional<DataManage> findOneById(String dataId);

    Optional<DataManage> findbyseoname(String dataId);

    Result<Page<DataManage>> findAllBySectionId(String sectionId , QueryDataManagePageRequest request);



    List<DataManage> findNewest(String sectionId);

    DataManage searchData(String title);

    DataManage save(DataManage data);


    /**
     * 热门标签
     * @param keyWord
     * @return
     */
    List<DataManage> hotTypeList(String keyWord);


    List<DataManage> hotNews(String section);

    Result<Page<DataManage>> findCaseList(String sectionId , QueryDataManagePageRequest request);

    List<DataManage> recommendNews(String sectionId);

    /**
     * 基本法页内容
     * @param id
     * @return
     */
    DataManage getBasciLaw(String id);

    /**
     * 新闻上下篇
     *
     * @param model, dataId
     * @author shen
     * @return org.springframework.ui.Model
     **/
    Model preAndNextDataManage(Model model, String dataId);

}
