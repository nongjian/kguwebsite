package com.kgu.data.impl;

import com.kgu.data.IModelAttributeService;
import com.kgu.data.ModelAttributeRepository;
import com.kgu.request.DataAttributeDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ModelAttributeServiceImpl implements IModelAttributeService {

    @Autowired
    ModelAttributeRepository modelAttributeRepository;

    @Override
    public List<DataAttributeDTO> findModelAttribute(String dataId, String modelId) {

        return modelAttributeRepository.findModelAttribute(dataId,modelId);
    }
}
