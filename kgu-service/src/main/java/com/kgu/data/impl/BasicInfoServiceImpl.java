package com.kgu.data.impl;


import com.kgu.data.BasicInfoRepository;
import com.kgu.data.IBasicInfoService;
import com.kgu.entity.basic.BasicInfo;
import com.kgu.entity.basic.BasicInfoDetailResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 15:27
 **/
@Service
@Slf4j
public class BasicInfoServiceImpl implements IBasicInfoService {

    @Autowired
    private BasicInfoRepository basicInfoRepository;


    @Override
    public BasicInfoDetailResponse detail() {
        BasicInfoDetailResponse response = new BasicInfoDetailResponse();
        BasicInfo basicInfoOptional = basicInfoRepository.findOneBySiteIdAndDeletedIsFalseAndAvailableIsTrue("68499e58090d4aafa0fb6d0545d4f2c1");
        if (basicInfoOptional!=null){
            BeanUtils.copyProperties(basicInfoOptional, response);
        }
        return response;
    }



}
