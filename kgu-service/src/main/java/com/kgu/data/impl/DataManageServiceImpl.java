package com.kgu.data.impl;

import com.kgu.SectionRepository;
import com.kgu.common.Result;
import com.kgu.data.DataManageRepository;
import com.kgu.data.IDataManageService;
import com.kgu.entity.data.DataManage;
import com.kgu.enums.SysLanguage;
import com.kgu.request.QueryDataManagePageRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.dom4j.rule.Mode;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import java.util.*;
import java.util.stream.Collectors;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-03-10 16:36
 **/
@Slf4j
@Service
public class DataManageServiceImpl implements IDataManageService {

    @Autowired
    private DataManageRepository dataManageRepository;

    @Autowired
    private SectionRepository sectionRepository;


    private List<String> pageNewsIds = new LinkedList<>();
    private String nextId;
    private String preId;


    @Override
    public List<DataManage> findAllByav() {
        return  dataManageRepository.findByDeletedIsFalseAndAvailableIsTrue();
    }

    @Override
    @Transactional
    public Result<Page<DataManage>> search(QueryDataManagePageRequest request) {

        Pageable pageable = PageRequest.of(request.getPageNum(), request.getPageSize(), Sort.Direction.DESC, "comparable");
        Specification<DataManage> specification = (Specification<DataManage>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = Lists.newArrayList();
            if (!StringUtils.isEmpty(request.getTitle())) {
                predicates.add(criteriaBuilder.like(root.get("title").as(String.class), "%" + request.getTitle() + "%"));
            }
            if (!StringUtils.isEmpty(request.getSectionId())) {
                predicates.add(criteriaBuilder.like(root.get("sectionId").as(String.class), "%" + request.getSectionId() + "%"));
            }
            predicates.add(criteriaBuilder.equal(root.get("deleted").as(Boolean.class), Boolean.FALSE));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<DataManage> dataManagePage = dataManageRepository.findAll(specification, pageable);
        return Result.<Page<DataManage>>builder().success().data(dataManagePage).build();
    }

    @Override
    public List<DataManage> searchList(String sectionId) {
        Long start = new Date().getTime();
        List<DataManage> list = dataManageRepository.findAllBySectionIdAndAvailableIsTrueAndDeletedIsFalse(sectionId);
//        System.out.println(sectionId);
//        System.out.println("数据查询时间：" + (new Date().getTime() - start));
        return list;
    }

    @Override
    public List<DataManage> queryExpandSection(String sectionId) {
        List<DataManage> list = dataManageRepository.queryExpandSection(sectionId);
        return list;
    }

    @Override
    public List<DataManage> searchList(String sectionId, String query) {
        Sort sort = Sort.by("comparable");
        Specification<DataManage> specification = (Specification<DataManage>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = Lists.newArrayList();
            if (!StringUtils.isEmpty(query)) {
                predicates.add(criteriaBuilder.like(root.get("type").as(String.class), "%" + query + "%"));
            }
            if (!StringUtils.isEmpty(sectionId)) {
                predicates.add(criteriaBuilder.equal(root.get("sectionId").as(String.class), sectionId));
            }
            predicates.add(criteriaBuilder.equal(root.get("deleted").as(Boolean.class), Boolean.FALSE));
            predicates.add(criteriaBuilder.equal(root.get("available").as(Boolean.class), Boolean.TRUE));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        return dataManageRepository.findAll(specification, sort);
    }

    @Override
    public List<DataManage> findBySectionIdAndLimit(String sectionId, Integer count) {
        return dataManageRepository.findAllBySectionIdAndLimit2(sectionId, count);
    }

    @Override
    public List<DataManage> findBySectionIdAndExpendSection(String sectionId, Integer count) {
        return dataManageRepository.findAllBySectionIdAndExpend(sectionId, count);
    }

    @Override
    public Result<Page<DataManage>> findAllBySectionIds(List<String> ids, QueryDataManagePageRequest request) {
        Pageable pageable = PageRequest.of(request.getPageNum(), request.getPageSize(), Sort.Direction.DESC, "comparable");
        Specification<DataManage> specification = (Specification<DataManage>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = Lists.newArrayList();
            if (!StringUtils.isEmpty(request.getTitle())) {
                predicates.add(criteriaBuilder.like(root.get("title").as(String.class), "%" + request.getTitle() + "%"));
            }
            if (!StringUtils.isEmpty(request.getType())) {
                predicates.add(criteriaBuilder.like(root.get("type").as(String.class), "%" + request.getType() + "%"));
            }
            if (ids != null) {
                predicates.add(root.get("sectionId").in(ids));
//                CriteriaBuilder.In<String> in = criteriaBuilder.in(root.get("sectionId").as(String.class));
//                ids.forEach(id -> {
//                    in.value(id);
//                });

//                in.value(request.getSectionId());
//                predicates.add(in);
            }
            predicates.add(criteriaBuilder.equal(root.get("deleted").as(Boolean.class), Boolean.FALSE));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<DataManage> page = dataManageRepository.findAll(specification, pageable);
        return new Result.Builder<Page<DataManage>>().data(page).build();
    }


    @Override
    public Optional<DataManage> findOneById(String dataId) {
        return dataManageRepository.findById(dataId);
    }

    @Override
    public Optional<DataManage> findbyseoname(String dataId) {
        return dataManageRepository.findBySeoname(dataId);
    }

    /**
     * 新闻页分页
     *
     * @param request
     * @return
     */
    @Override
    public Result<Page<DataManage>> findAllBySectionId(String sectionId, QueryDataManagePageRequest request) {
        Sort sort = Sort.by(Sort.Direction.DESC, "comparable").and(Sort.by(Sort.Direction.DESC,"createTime"));
        Pageable pageable = PageRequest.of(request.getPageNum(), request.getPageSize(), sort);
        Specification<DataManage> specification = (Specification<DataManage>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = Lists.newArrayList();
            if (!StringUtils.isEmpty(request.getTitle()) && request.getTitle() != "") {
                predicates.add(criteriaBuilder.like(root.get("title").as(String.class), "%" + request.getTitle() + "%"));
            }
            predicates.add(criteriaBuilder.equal(root.get("sectionId").as(String.class), sectionId));
            predicates.add(criteriaBuilder.equal(root.get("deleted").as(Boolean.class), Boolean.FALSE));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        Page<DataManage> page = dataManageRepository.findAll(specification, pageable);
        List<DataManage> content = page.getContent();
        List<String> pageNewsIds = content.stream().map(dataManage -> dataManage.getId()).collect(Collectors.toList());
        //给新闻页ids赋值
        this.pageNewsIds = pageNewsIds;


        return new Result.Builder<Page<DataManage>>().data(page).build();
    }

    @Override
    public List<DataManage> findNewest(String sectionId) {
        List<DataManage> list = dataManageRepository.findNewest(sectionId);
        return list;
    }


    /**
     * 新闻详情 上一篇 与 下一篇
     *
     * @param
     * @return
     */

    @Override
    public Model preAndNextDataManage(Model model,String dataId){
        nextId = null;
        preId = null;
        if (this.pageNewsIds != null) {
            for (int i = 0; i < this.pageNewsIds.size(); i++) {
                if (this.pageNewsIds.size() == 1){

                } else {
                    if (dataId.equals(this.pageNewsIds.get(i))) {
                        if (i == 0) {
                            preId = this.pageNewsIds.get(0);
                            nextId = this.pageNewsIds.get(i+1);
                        } else if(i == this.pageNewsIds.size() - 1){
                            preId = this.pageNewsIds.get(i - 1);
                            nextId = this.pageNewsIds.get(0);
                        } else {
                            preId = this.pageNewsIds.get(i - 1);
                            nextId = this.pageNewsIds.get(i + 1);
                        }
                    }
                }

            }
        }
        if (nextId != null && preId != null){
            DataManage beforeDataManage = dataManageRepository.findAllByIdAndDeletedIsFalse(preId);
            DataManage afterDataManage = dataManageRepository.findAllByIdAndDeletedIsFalse(nextId);
            model.addAttribute("before", beforeDataManage);
            model.addAttribute("after", afterDataManage);
        } else {
            DataManage beforeDataManage = dataManageRepository.next(dataId);
            DataManage afterDataManage = dataManageRepository.last(dataId);
            model.addAttribute("before", beforeDataManage);
            model.addAttribute("after", afterDataManage);
        }

        return  model;
    }


    @Override
    public DataManage searchData(String title) {
        return dataManageRepository.searchData(title);
    }

    @Override
    public DataManage save(DataManage data) {

        return dataManageRepository.save(data);
    }



    /**
     * 热门标签 -->对应的数据
     * @param keyWord
     * @return
     */
    @Override
    public List<DataManage> hotTypeList(String keyWord) {
        return dataManageRepository.hotTypeList(keyWord);
    }

    @Override
    public List<DataManage> hotNews(String section) {
        return dataManageRepository.hotNews(section);
    }

    @Override
    public Result<Page<DataManage>> findCaseList(String sectionId, QueryDataManagePageRequest request) {
        Sort sort = Sort.by(Sort.Direction.DESC, "comparable").and(Sort.by(Sort.Direction.DESC,"createTime"));
        Pageable pageable = PageRequest.of(request.getPageNum(), request.getPageSize(),   sort);
        Specification<DataManage> specification = (Specification<DataManage>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> listAnd = new ArrayList<Predicate>();
            List<Predicate> listOr = new ArrayList<Predicate>();
            Predicate[] array_and=new Predicate[listAnd.size()];
            if (!StringUtils.isEmpty(request.getTitle())) {
                listAnd.add(criteriaBuilder.like(root.get("title").as(String.class), "%" + request.getTitle() + "%"));
            }
            if (!StringUtils.isEmpty(request.getType())) {
                listAnd.add(criteriaBuilder.like(root.get("type").as(String.class), "%" + request.getType() + "%"));
            } else {
                listAnd.add(criteriaBuilder.equal(root.get("sectionId").as(String.class), sectionId));
            }
            listAnd.add(criteriaBuilder.equal(root.get("sectionId").as(String.class), "2c9180887945b521017945c2862a0066"));
            listAnd.add(criteriaBuilder.equal(root.get("deleted").as(Boolean.class), Boolean.FALSE));
            Predicate Pre_And = criteriaBuilder.and(listAnd.toArray(array_and));
            return criteriaQuery.where(Pre_And).getRestriction();
        };
        Page<DataManage> page = dataManageRepository.findAll(specification,pageable);
        Result<Page<DataManage>> result = new Result.Builder<Page<DataManage>>().data(page).build();
        return result;
    }

    /**
     * 推荐新闻
     * @param sectionId
     * @return
     */
    @Override
    public List<DataManage> recommendNews(String sectionId) {
        return dataManageRepository.recommendList(sectionId);
    }

    /**
     * 基本法内容
     * @param id
     * @return
     */
    @Override
    public DataManage getBasciLaw(String id) {
        return dataManageRepository.getBasciLaw(id);
    }


}
