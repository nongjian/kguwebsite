package com.kgu.data;

import com.kgu.request.DataAttributeDTO;

import java.util.List;

public interface IModelAttributeService {

    List<DataAttributeDTO> findModelAttribute(String dataId, String modelId);

}
