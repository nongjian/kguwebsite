package com.kgu.data;


import com.kgu.entity.basic.BasicInfoDetailResponse;
import org.springframework.cache.annotation.Cacheable;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 14:47
 **/
public interface IBasicInfoService {

    /**
     * 查看登录站点的基本信息
     * @return
     */
//    @Cacheable(value = "kguBasicinfo")
    BasicInfoDetailResponse detail();


}
