package com.kgu.cache;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;

public interface ICacheService {

    public void setClientCache(String key, Serializable object);

    /**
     * 设置client客户端缓存
     * @param key
     */
    public void setClientCache(String key, Serializable object, Integer time, TimeUnit timeUnit);

    /**
     * 删除client客户端缓存
     * @param key
     */
    public void removeClientCache(String key);
    /**
     * 获取client客户端缓存
     * @param key
     */
    public Serializable getClientCache(String key);
    /**
     * 设置自定义缓存
     * @param key
     * @param object
     * @param prefix 前缀
     */
    public void setCache(String key,Serializable object,String prefix);

    public void setCacheList(String key, List<Serializable> data, String prefix);

    public List<Serializable> getCacheList(String key, String prefix);

    /**
     * 删除自定义缓存
     * @param key
     * @param prefix 前缀
     */
    public void removeCache(String key,String prefix);

    /**
     * 获取自定义缓存
     * @param key
     * @param prefix 前缀
     */
    public Serializable getCache(String key,String prefix);
}
