package com.kgu.cache.impl;

import com.kgu.cache.ICacheService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
public class CacheServiceImpl implements ICacheService {

    //客户端
    public static final String CLIENT_CACHE_PREFIX = "CACHE:";

    public static final String VALIDATE_CODE = "VALIDATE_CODE:";

    @Autowired
    private RedisTemplate<String, Serializable> redisTemplate;

    /**
     * 设置client客户端缓存
     * @param key
     */
    public void setClientCache(String key,Serializable object) {
        redisTemplate.opsForValue().set(CLIENT_CACHE_PREFIX+key, object);
    }

    /**
     * 设置client客户端缓存
     * @param key
     */
    public void setClientCache(String key, Serializable object, Integer time, TimeUnit timeUnit) {
        redisTemplate.opsForValue().set(CLIENT_CACHE_PREFIX+key, object,time,timeUnit);
    }

    /**
     * 删除client客户端缓存
     * @param key
     */
    public void removeClientCache(String key){
        redisTemplate.delete(CLIENT_CACHE_PREFIX+key);
    }

    /**
     * 获取client客户端缓存
     * @param key
     */
    public Serializable getClientCache(String key){
        return redisTemplate.opsForValue().get(CLIENT_CACHE_PREFIX+key);
    }

    /**
     * 设置自定义缓存
     * @param key
     * @param object
     * @param prefix 前缀
     */
    public void setCache(String key,Serializable object,String prefix) {
        redisTemplate.opsForValue().set(prefix+key, object);
    }

    public void setCacheList(String key, List<Serializable> data, String prefix) {
        redisTemplate.opsForList().leftPushAll(prefix+key,data);
    }

    public List<Serializable> getCacheList(String key, String prefix) {
        return redisTemplate.opsForList().range(prefix+key,0,-1);
    }

    /**
     * 删除自定义缓存
     * @param key
     * @param prefix 前缀
     */
    public void removeCache(String key,String prefix){
        redisTemplate.delete(prefix+key);
    }

    /**
     * 获取自定义缓存
     * @param key
     * @param prefix 前缀
     */
    public Serializable getCache(String key,String prefix){
        return redisTemplate.opsForValue().get(prefix+key);
    }
}
