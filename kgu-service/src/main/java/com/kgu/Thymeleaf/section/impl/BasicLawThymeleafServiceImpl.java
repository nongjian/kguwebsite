package com.kgu.Thymeleaf.section.impl;

import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.data.BasicRuleRepository;
import com.kgu.data.IBasicInfoService;
import com.kgu.data.IDataManageService;
import com.kgu.entity.basic.BasicRule;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.xml.crypto.Data;
import java.util.List;

@Service(value = "basicLaw")
public class BasicLawThymeleafServiceImpl extends SectionThymeleafService {
    @Autowired
    IBasicInfoService basicService;

    @Autowired
    BasicRuleRepository basicRuleRepository;
    @Autowired
    IDataManageService dataManageService;

    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);

        List<BasicRule> basicRuleList = basicRuleRepository.getAll();
        model.addAttribute("basicRuleList",basicRuleList);

        getBasciLaw(model,sectionPage);
        return "/pages/talents/basicLaw";
    }
    public Model getBasciLaw (Model model,Section sectionPage){
        DataManage basciLaw = dataManageService.getBasciLaw(sectionPage.getId());
        model.addAttribute("basicLaw",basciLaw);
        return  model;
    }



}
