package com.kgu.Thymeleaf.section;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SectionThymeleafStrategy {

    @Autowired
    Map<String,SectionThymeleafService>stringSectionThymeleafServiceMap=new ConcurrentHashMap<>();

    public SectionThymeleafService sectionThymeleaf(String sectionTemplate){
        return stringSectionThymeleafServiceMap.get(sectionTemplate);
    }

}
