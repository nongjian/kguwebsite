package com.kgu.Thymeleaf.section.impl;

import com.kgu.PageUtil;
import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.Utils;
import com.kgu.common.Result;
import com.kgu.data.IDataManageService;
import com.kgu.data.IModelAttributeService;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import com.kgu.request.QueryDataManagePageRequest;
import com.kgu.section.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;
import java.util.stream.Collectors;

@Service(value = "case")
public class CaseThymeleafServiceImpl extends SectionThymeleafService {


    @Autowired
    IDataManageService iDataManageService;

    @Autowired
    SectionService sectionService;

    @Autowired
    IModelAttributeService iModelAttributeService;


    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        caseList(model,sectionPage,sectionList);
        return "/pages/case/case";
    }

    public Model caseList(Model model,Section section, List<Section> sectionList){
        List<Section> allSection = sectionService.findAll();
        QueryDataManagePageRequest query = new QueryDataManagePageRequest();
        query.setSectionId(section.getId());
        query.setPageNum((Integer) parameter[0]);
        query.setPageSize((Integer) parameter[1]);
        query.setTitle(caseTitle);
        query.setType(str);
        Result<Page<DataManage>> pageResult = iDataManageService.findCaseList(section.getId(),query);

        List<DataManage> list = pageResult.getData().getContent();
        list.forEach(dataManage -> {
            dataManage.setModelAttributes(iModelAttributeService.findModelAttribute(dataManage.getId(),section.getModelId()));
        });
        model.addAttribute("phoneList", Utils.label(allSection,list));
        List<Integer> list1 = PageUtil.pageInfo((Integer)parameter[0],pageResult.getData().getTotalPages());
        model.addAttribute("page",list1);
        model.addAttribute("caseList",pageResult);

        return model;
    }
}
