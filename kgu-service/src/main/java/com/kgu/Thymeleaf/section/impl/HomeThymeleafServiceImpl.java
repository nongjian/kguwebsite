package com.kgu.Thymeleaf.section.impl;

import com.kgu.ContextVariable;
import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.Utils;
import com.kgu.data.IDataManageService;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;
import com.kgu.section.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @Author vien
 * @Description 主页
 * @Date 14:26 2021/5/1
 * @Param
 * @return
 **/
@Service(value = "home")
public class HomeThymeleafServiceImpl extends SectionThymeleafService {

    @Autowired
    private IDataManageService iDataManageService;

    @Autowired
    SectionService sectionService;

    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        List<Section> allSection = ContextVariable.getAllSection();
//从这里注入 type = sectionList.getid
    sectionList.forEach(section -> {
            List<DataManage> dataManageList = iDataManageService.findBySectionIdAndLimit(section.getId(), section.getIndexNo());
            section.setDatas(Utils.label(allSection, dataManageList));
        });
        sectionList.forEach(section -> {
            if ("news".equals(section.getTemplate())){
                List<DataManage> list = iDataManageService.findNewest(section.getId());
                section.setDatas(list);
                model.addAttribute("homenew",section);
            }
        });
        return "/pages/home/home";
    }

}
