package com.kgu.Thymeleaf.section.impl;

import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.data.DataManageRepository;
import com.kgu.data.IDataManageService;
import com.kgu.data.IModelAttributeService;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import com.kgu.request.DataAttributeDTO;
import com.kgu.section.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

@Service(value = "detail")
public class DetailThymeleafServiceImpl extends SectionThymeleafService {

    @Autowired
    private IDataManageService iDataManageService;
    @Autowired
    private SectionService sectionService;

    @Autowired
    private DataManageRepository dataManageRepository;

    @Autowired
    IModelAttributeService iModelAttributeService;

    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        Optional<DataManage> dataManage = iDataManageService.findOneById(parameter[0].toString());

        List<DataAttributeDTO> modelAttributes = iModelAttributeService.findModelAttribute(dataManage.get().getId(),sectionPage.getModelId());

        List<DataManage> dataManagePage = dataManageRepository.findCaseList(dataManage.get().getId(),null,null);

        String[] sectionIds = new String[10];
        if (dataManage.get().getType()!=null) {
            sectionIds = dataManage.get().getType().split(",");
        }
        List<Section> allSection = sectionService.findAll();
        String[] finalSectionIds = sectionIds;
        List<Section> list = allSection.stream().filter(section -> {return Arrays.asList(finalSectionIds).contains(section.getId()) && section.getParentId() != null ;}).collect(Collectors.toList());
        Map<String,String> map = list.stream().collect(Collectors.toMap(Section::getId,Section::getTitle,(s1, s2)->s2));
        dataManage.get().setLabelMap(map);
        dataManage.get().setModelAttributes(modelAttributes);
        DataManage dataManage1 = dataManage.get();
        String qrCode = dataManage1.getQrCode();
        String qrtwoCode = dataManage1.getQrtwoCode();
        if (qrCode != null && !qrCode.equals("")){
            Section one = sectionService.getOne(qrCode);
            if (one != null){
                if (one.getTitle()!= null && !"".equals(one.getTitle())){
                    dataManage.get().setQrCode(one.getTitle());
                }
            }
        }
        if (qrtwoCode != null && !qrCode.equals("")){
            Section one = sectionService.getOne(qrtwoCode);
            if (one != null){
                if (one.getTitle()!= null && !"".equals(one.getTitle())){
                    dataManage.get().setQrtwoCode(one.getTitle());
                }
            }
        }


        model.addAttribute("detail",dataManage.get());
        model.addAttribute("relevance",dataManagePage);
        if (StringUtils.isEmpty(parameter[1])){
            parameter[1] = "case";
        }
        return "/pages/"+parameter[1].toString()+"/"+parameter[1].toString()+"detail";
    }
}
