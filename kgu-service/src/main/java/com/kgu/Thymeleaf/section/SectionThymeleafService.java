package com.kgu.Thymeleaf.section;

import com.kgu.ContextVariable;
import com.kgu.SectionRepository;
import com.kgu.Utils;
import com.kgu.data.IBasicInfoService;
import com.kgu.data.IDataManageService;
import com.kgu.data.impl.BasicInfoServiceImpl;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;
import com.kgu.tag.ITagService;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import java.util.*;
import java.util.stream.Collectors;

public abstract class SectionThymeleafService implements BeanFactoryAware {


    private static BeanFactory beanFactory = null;
    private static SectionThymeleafService serviceLocator = null;

    public Object[] parameter;
    public Object[] phoneParameter = {0};
    public String str;
    public String caseTitle;

    @Autowired
    SectionRepository sectionRepository;
    @Autowired
    IDataManageService dataManageService;

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        SectionThymeleafService.beanFactory = beanFactory;
    }

    public static SectionThymeleafService getInstance() {
        if (serviceLocator == null) {
            serviceLocator = (SectionThymeleafService) beanFactory.getBean("serviceLocator");
        }
        return serviceLocator;
    }

    public static Object getService(Class serviceName) {
        return beanFactory.getBean(serviceName);
    }

    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        fullData(model, sectionList, sectionPage);
        model.addAttribute("sectionList", sectionList);
        groudbycertificate(sectionPage, model);
        model.addAttribute("sectionPage", sectionPage);
        basicInfo(model);
        allTag(model);
        sectionTag(model, sectionPage);
        hotNews(model);
        model.addAttribute("dateTime", new Date().getTime() / 36e5);

        return "";
    }

    public void groudbycertificate(Section sectionList, Model model) {
        Map<String, Tag> map = sectionList.getSectionTags();
        List<Tag> tagList = new ArrayList<>();
        map.forEach((x, tag) -> { tagList.add(tag); });
        List<List<Tag>> stringListMap = new ArrayList<>();
        List<Tag> tagLists = new ArrayList<>();
        for(int i=0;i<tagList.size();i++){
            Tag x = tagList.get(i);
            if (x.getName().indexOf("aboutImg") != -1) {
                tagLists.add(x);
                if(tagLists.size()==2||i==tagList.size()){
                    stringListMap.add(tagLists);
                    tagLists = new ArrayList<>();
                }
            }
        }
        model.addAttribute("aboutimgs", stringListMap);
    }

    public SectionThymeleafService setParameter(Object... parameter) {
        this.parameter = parameter;
        return this;
    }

    public SectionThymeleafService setPhoneParameter(Object... parameter) {
        this.phoneParameter = parameter;
        return this;
    }

    public SectionThymeleafService setQueryParameter(String parameter) {
        this.str = parameter;
        return this;
    }

    public SectionThymeleafService setCaseQueryTitle(String parameter) {
        this.caseTitle = parameter;
        return this;
    }

    public void fullData(Model model, List<Section> sectionList, Section sectionPage) {
        IDataManageService iDataManageService = (IDataManageService) getService(IDataManageService.class);
        List<Section> allSection = ContextVariable.getAllSection();
        List<Tag> list = ContextVariable.getAllTag();
        Map<String, List<Tag>> map = list.stream().collect(Collectors.groupingBy(Tag::getSectionId));
        if (map.size() != 0 && map.get(sectionPage.getId()) != null) {
            List<DataManage> secrionPageDataList = iDataManageService.searchList(sectionPage.getId(), str);
            List<DataManage> expend = iDataManageService.queryExpandSection(sectionPage.getId());
            secrionPageDataList.addAll(expend);
            List<DataManage> sortList = secrionPageDataList.stream()
                    .sorted(Comparator.comparing(DataManage::getCreateTime))
                    .sorted(Comparator.comparing(DataManage::getComparable))
                    .filter(data -> {
                        return !"2c9180887945b521017945c40610008a".equals(data.getSectionId());
                    })
                    .collect(Collectors.toList());
            sectionPage.setDatas(Utils.label(allSection, sortList));
            model.addAttribute("dataList", sortList);
            sectionPage.setSectionTags(map.get(sectionPage.getId()).stream().collect(Collectors.toMap(Tag::getName, a -> a, (a1, a2) -> a2)));
        }
        sectionList.forEach(section -> {
            if (map.size() != 0 && map.get(section.getId()) != null) {
                section.setSectionTags(map.get(section.getId())
                        .parallelStream()
                        .collect(Collectors.toMap(Tag::getName, a -> a, (a1, a2) -> a2)));
            }
        });
    }

    public Model basicInfo(Model model) {
        IBasicInfoService iBasicInfoService = (IBasicInfoService) SectionThymeleafService.getService(BasicInfoServiceImpl.class);
        model.addAttribute("basic", iBasicInfoService.detail());
        return model;
    }

    public Model allTag(Model model) {
        ITagService iTagService = (ITagService) SectionThymeleafService.getService(ITagService.class);
        List<Tag> list = iTagService.search();
        Map<String, Tag> map = list.stream().collect(Collectors.toMap(Tag::getName, a -> a, (a1, a2) -> a2));
        model.addAttribute("allTag", map);
        return model;
    }

    public Model sectionTag(Model model, Section sectionPage) {
        ITagService iTagService = (ITagService) SectionThymeleafService.getService(ITagService.class);
        List<Tag> list = iTagService.sectionTag(sectionPage.getId());
        Map<String, Tag> map = list.stream().collect(Collectors.toMap(Tag::getName, a -> a, (a1, a2) -> a2));
        model.addAttribute("sectionTag", map);
        return model;
    }

    public Model hotNews(Model model) {
        Long start = new Date().getTime();
        List<DataManage> dataManages = dataManageService.recommendNews("2c9180887945b521017945c40610008a");
        model.addAttribute("hotNews", dataManages);
        return model;
    }

}
