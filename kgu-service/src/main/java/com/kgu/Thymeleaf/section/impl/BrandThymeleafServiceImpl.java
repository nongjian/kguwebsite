package com.kgu.Thymeleaf.section.impl;

import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.entity.section.Section;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service(value = "brand")
public class BrandThymeleafServiceImpl extends SectionThymeleafService {
    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        return "/pages/brand/brand";
    }
}
