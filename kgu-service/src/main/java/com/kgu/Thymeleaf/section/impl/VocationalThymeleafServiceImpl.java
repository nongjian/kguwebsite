package com.kgu.Thymeleaf.section.impl;

import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.data.DataManageRepository;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;
import com.kgu.tag.TagRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service(value = "vocational")
public class VocationalThymeleafServiceImpl extends SectionThymeleafService {
    @Autowired
    TagRepository tagRepository;
    @Autowired
    DataManageRepository dataManageRepository;
    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        List<DataManage> dataManageList = dataManageRepository.findAllBySectionIdAndAvailableIsTrueAndDeletedIsFalse(sectionPage.getId());
        List<DataManage> sortList=dataManageList.stream().sorted(Comparator.comparing(DataManage::getCreateTime)).sorted(Comparator.comparing(DataManage::getComparable)).collect(Collectors.toList());
        List<DataManage> collect = sortList.stream().limit(12).collect(Collectors.toList());
        model.addAttribute("dataList",collect);
        return "/pages/inter+/vocational";
    }
}
