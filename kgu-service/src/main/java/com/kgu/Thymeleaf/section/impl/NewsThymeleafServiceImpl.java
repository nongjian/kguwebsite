package com.kgu.Thymeleaf.section.impl;

import com.kgu.PageUtil;
import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.common.Result;
import com.kgu.data.IBasicInfoService;
import com.kgu.data.IDataManageService;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import com.kgu.request.QueryDataManagePageRequest;
import org.apache.commons.compress.utils.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service(value = "news")
public class NewsThymeleafServiceImpl extends SectionThymeleafService {

    @Autowired
    IDataManageService dataManageService;

    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        newsList(model,sectionPage);
        return "/pages/news/news";
    }


    public Model newsList(Model model,Section section){
        QueryDataManagePageRequest query = new QueryDataManagePageRequest();
        query.setSectionId(section.getId());
        query.setPageNum((Integer) parameter[0]);
        query.setPageSize(12);
        query.setTitle(caseTitle);
        Result<Page<DataManage>> newsList = dataManageService.findAllBySectionId(section.getId(),query);
        List<Integer> list1 = PageUtil.pageInfo((Integer)parameter[0],newsList.getData().getTotalPages());
        model.addAttribute("page",list1);
        model.addAttribute("newsList",newsList);
        return model;
    }




}
