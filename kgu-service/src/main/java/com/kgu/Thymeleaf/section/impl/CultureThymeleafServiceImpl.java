package com.kgu.Thymeleaf.section.impl;

import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;
import com.kgu.tag.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Map;

@Service(value = "culture")
public class CultureThymeleafServiceImpl extends SectionThymeleafService {

    @Autowired
    ITagService tagService;


    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        List<Tag> tagList = tagService.sectionTag("2c9180887945b521017945d8313200da");
        model.addAttribute("tagList",tagList);
        return "/pages/talents/culture";
    }
}
