package com.kgu.Thymeleaf.section.impl;

import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.Utils;
import com.kgu.data.DataManageRepository;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;
import com.kgu.section.SectionService;
import com.kgu.tag.ITagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service(value = "inter+")
public class InterThymeleafServiceImpl extends SectionThymeleafService {

    @Autowired
    DataManageRepository  dataManageRepository;
    @Autowired
    SectionService sectionService;
    @Autowired
    ITagService tagService;

    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);

        List<DataManage> dataManageList = dataManageRepository.findBySectionIdAndDeletedIsFalse(sectionPage.getId());
        model.addAttribute("dataManageList",dataManageList);

        getAllSonMessage(model,sectionPage);

        return "/pages/inter+/inter+";
    }

    /**
     * inter+ 获取子栏目信息
     */


    public Model getAllSonMessage(Model model, Section sectionPage) {
        List<Section> sections = sectionPage.getChildren();

        sections.forEach(section -> {
            List<Section> allSection = sectionService.findAll();
            List<DataManage> dataManageList = dataManageRepository.findAllBySectionIdAndAvailableIsTrueAndDeletedIsFalse(section.getId());
            section.setDatas(Utils.label(allSection,dataManageList));
            List<Tag> tagList = tagService.sectionTag(section.getId());
            Map<String, Tag> tagMap = tagList.parallelStream()
                    .collect(Collectors.toMap(Tag::getName, a -> a, (a1, a2) -> a2));
            section.setSectionTags(tagMap);
        });
        model.addAttribute("section", sections);
        return model;
    }
}
