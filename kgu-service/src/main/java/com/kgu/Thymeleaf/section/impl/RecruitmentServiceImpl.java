package com.kgu.Thymeleaf.section.impl;

import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.common.Position;
import com.kgu.emp.IRecruitmentService;
import com.kgu.entity.emp.Recruitment;
import com.kgu.entity.section.Section;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.List;

@Service(value = "recruitment")
public class RecruitmentServiceImpl extends SectionThymeleafService {


    @Autowired
    IRecruitmentService  recruitmentService;


    @Override
    public String section(Model model, List<Section> sectionList, Section sectionPage) {
        super.section(model,sectionList,sectionPage);
        List<Recruitment> workCityList = recruitmentService.getWorkCityList();
        if (parameter[0].toString().equals("0")){
            parameter[0] = "苏州";
        }
        List<Recruitment> positionNameList = recruitmentService.getPositionNameList(parameter[0].toString());
        boolean flag = false;
        for (Recruitment recruitment : positionNameList) {
            if (parameter[1].toString().equals(recruitment.getPositionName())){
                flag = true;
            }
        }
        if (!flag){
            parameter[1] = positionNameList.get(0).getPositionName();
        }
        model.addAttribute("phoneParam",phoneParameter);
        model.addAttribute("phoneList",0);
//        List<Recruitment> recruitments = recruitmentService.findAllByWorkCityAndPositionNameAndDeletedIsFalse(parameter[0].toString(), parameter[1].toString());
        List<Recruitment> recruitments = recruitmentService.findAllByWorkCity(parameter[0].toString());


        //手机端城市定位获取
        String address = (String)model.getAttribute("address");
        if (phoneParameter !=null && address != null){
            phoneList(model,address);
        }else {
            model.addAttribute("phoneCityList",workCityList);
            List<Recruitment>  phoneList = recruitmentService.phoneList(parameter[0].toString());
            model.addAttribute("phoneList",phoneList);
        }
        model.addAttribute("parameter",parameter);
        model.addAttribute("recruitments",recruitments);
        model.addAttribute("workCityList",workCityList);
        model.addAttribute("positionNameList",positionNameList);
        return "/pages/talents/recruitment";
    }

    public Model phoneList(Model model, String position){
        //获取手机端 城市列表
        List<Recruitment> workCityList = recruitmentService.getWorkCityList();
        setPhoneParameter(position);
        model.addAttribute("phoneCityList",workCityList);
        model.addAttribute("ajaxPhoneCityList",workCityList);
        model.addAttribute("phoneParam",phoneParameter);
        //获取手机端 工作列表
        List<Recruitment>  phoneList = recruitmentService.phoneList(position);
        model.addAttribute("phoneList",phoneList);
        return  model;
    }



}



























