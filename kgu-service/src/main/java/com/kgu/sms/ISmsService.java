package com.kgu.sms;

public interface ISmsService {

    boolean send(String phone, String randomCode) throws  Exception;

}
