package com.kgu.link.impl;

import com.kgu.common.Result;
import com.kgu.entity.link.LinkInfo;
import com.kgu.link.ILinkService;
import com.kgu.link.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LinkServiceImpl implements ILinkService {

    @Autowired
    LinkRepository linkRepository;

    @Override
    public Result<String> add(LinkInfo linkInfo) {
        LinkInfo re = linkRepository.save(linkInfo);
        if (re!=null){
            return new Result.Builder<String>().data("SUCCESS").code(200).build();
        }else {
            return new Result.Builder<String>().data("error").code(200).build();
        }
    }
}
