package com.kgu.link;

import com.kgu.common.Result;
import com.kgu.entity.link.LinkInfo;

public interface ILinkService {

    Result<String> add(LinkInfo linkInfo);

}
