package com.kgu.emp.impl;

import com.kgu.emp.IRecruitmentService;
import com.kgu.emp.RecruitmentRepository;
import com.kgu.entity.emp.Recruitment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class IRecruitmentServiceImpl implements IRecruitmentService {

    @Autowired
    RecruitmentRepository  recruitmentRepository;


    @Override
    public List<Recruitment> getWorkCityList() {
        return recruitmentRepository.getWorkCityList();
    }

    @Override
    public List<Recruitment> findAllBySiteIdAndPositionNameAndDeletedIsFalse(String workCity, String PositionName) {
        return recruitmentRepository.findAllBySiteIdAndPositionNameAndDeletedIsFalse(workCity,PositionName);
    }

    @Override
    public List<Recruitment> getPositionNameList(String siteId) {
        return recruitmentRepository.getPositionNameList(siteId);
    }

    @Override
    public Recruitment findOneById(String id) {
        return recruitmentRepository.findOneById(id);
    }

    @Override
    public List<Recruitment> findAllByWorkCityAndPositionNameAndDeletedIsFalse(String siteId, String PositionName) {
        return recruitmentRepository.findAllByWorkCityAndPositionNameAndDeletedIsFalse(siteId,PositionName);
    }

    @Override
    public List<Recruitment> phoneList(String workCity) {
        return recruitmentRepository.findAllByWorkCityAndDeletedIsFalseAndAvailableIsTrue(workCity);
    }

    @Override
    public Recruitment findByWorkCity(String workCity) {
        return recruitmentRepository.findByWorkCity(workCity);
    }

    @Override
    public List<Recruitment> findAllByWorkCity(String workCity) {
        return recruitmentRepository.findAllByWorkCity(workCity);    }
}
