package com.kgu.emp;

import com.kgu.entity.emp.Recruitment;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * 招聘
 */
public interface  IRecruitmentService{

    List<Recruitment> getWorkCityList();

    List<Recruitment> findAllBySiteIdAndPositionNameAndDeletedIsFalse(String  workCity, String PositionName);

    List<Recruitment> getPositionNameList(String siteId);

    Recruitment findOneById(String id);


    List<Recruitment> findAllByWorkCityAndPositionNameAndDeletedIsFalse(String  siteId,String PositionName);

    List<Recruitment>  phoneList(String workCity);

    Recruitment findByWorkCity(String workCity);

    List<Recruitment> findAllByWorkCity(String workCity);

}
