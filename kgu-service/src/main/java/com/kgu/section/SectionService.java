package com.kgu.section;

import com.kgu.entity.section.Section;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

public interface SectionService {

    @Cacheable(value = "allSection")
    List<Section> findAll();

    @Cacheable(value = "allSection")
    List<Section> findAlls();
    Section getOne(String id);
}
