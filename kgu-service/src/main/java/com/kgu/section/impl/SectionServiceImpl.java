package com.kgu.section.impl;

import com.kgu.SectionRepository;
import com.kgu.entity.section.Section;
import com.kgu.section.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SectionServiceImpl implements SectionService {
    @Autowired
    private SectionRepository sectionRepository;

    @Override
    public List<Section> findAll() {
        return sectionRepository.findAll();
    }


    @Override
    public List<Section> findAlls() {
        return sectionRepository.findAllByAvailableIsTrueAndDeletedIsFalse();
    }
    @Override
    public Section getOne(String id) {
        return sectionRepository.findOneById(id);
    }
}
