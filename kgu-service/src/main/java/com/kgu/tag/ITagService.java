package com.kgu.tag;

import com.kgu.common.Result;
import com.kgu.entity.tag.Tag;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;

import java.util.List;


/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 16:08
 **/
public interface ITagService {

    /**
     * 分页查看登录站点的标签
     * @return
     */
    List<Tag> search();

    List<Tag> sectionTag(String sectionId);

    @Cacheable(value = "tag")
    List<Tag> findAll();
}
