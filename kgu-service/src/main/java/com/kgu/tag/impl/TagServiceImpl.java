package com.kgu.tag.impl;

import com.kgu.SectionRepository;
import com.kgu.entity.tag.Tag;
import com.kgu.tag.ITagService;
import com.kgu.tag.TagRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.utils.Lists;
import org.hibernate.query.criteria.internal.expression.SimpleCaseExpression;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.List;


/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 16:20
 **/
@Slf4j
@Service
public class TagServiceImpl implements ITagService {

    @Autowired
    private TagRepository tagRepository;

    @Override
    public List<Tag> search() {
        Specification<Tag> specification = (Specification<Tag>) (root, criteriaQuery, criteriaBuilder) -> {
            List<Predicate> predicates = Lists.newArrayList();
            predicates.add(criteriaBuilder.isNull(root.get("sectionId")));
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
        List<Tag> tagPage = tagRepository.findAll(specification);
        return tagPage;
    }

    @Override
    public List<Tag> sectionTag(String sectionId) {
        return tagRepository.findBySectionIdAndDeletedIsFalseAndAvailableIsTrue(sectionId);
    }

    @Override
    public List<Tag> findAll() {
        return tagRepository.findAllBySectionIdNotNullAndDeletedIsFalseAndAvailableIsTrue();
    }

}
