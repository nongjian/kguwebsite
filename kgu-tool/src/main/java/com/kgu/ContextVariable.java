package com.kgu;

import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;

import java.util.List;
import java.util.Map;

public class ContextVariable {

    private static List<Section> allSection;
    private static List<Tag> allTag;

    private ContextVariable() {
    }

    private static final ThreadLocal<Map<String,String[]>> context = new ThreadLocal<Map<String,String[]>>();

    public static void set(Map<String,String[]> map) {
        context.set(map);
    }

    public static Map<String,String[]> get() {
        return context.get();
    }

    public static void remove() {
        context.remove();
    }

    public static List<Section> getAllSection() {
        return ContextVariable.allSection;
    }

    public static void setAllSection(List<Section> allSection) {
        ContextVariable.allSection = allSection;
    }

    public static List<Tag> getAllTag() {
        return ContextVariable.allTag;
    }

    public static void setAllTag(List<Tag> allTag) {
        ContextVariable.allTag = allTag;
    }
}
