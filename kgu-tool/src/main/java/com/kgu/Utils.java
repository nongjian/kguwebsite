package com.kgu;

import com.kgu.entity.data.DataManage;
import com.kgu.entity.section.Section;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Utils {

    public static List<DataManage> label(List<Section> allSection,List<DataManage> dataList){
        dataList.forEach(d->{
            if (!StringUtils.isEmpty(d.getType())){
                String[] sectionIds = d.getType().split(",");
                List<Section> list = allSection.stream().filter(section -> {return Arrays.asList(sectionIds).contains(section.getId()) && section.getParentId() != null ;}).collect(Collectors.toList());

                Map<String,String> map = list.stream().collect(Collectors.toMap(Section::getId,Section::getTitle,(s1,s2)->s2));
                d.setLabelMap(map);
            }
        });
        return dataList;
    }

}
