package com.kgu;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// 分页工具类
public class PageUtil<T> {

    public static List<Integer> pageInfo(Integer currentPage,Integer totalPage){
        Integer count = 4;
        List<Integer> list = new ArrayList<>();
        for (int a=1;a<=4;a++){
            if (list.size()==count){
                break;
            }
            if ((currentPage-a)>=0){
                list.add(currentPage-a);
            }
            if ((currentPage+a)<totalPage){
                list.add(currentPage+a);
            }
        }
        list.add(currentPage);
        list.sort((a,b)->{ return a.compareTo(b); });
        return list;
    }
}