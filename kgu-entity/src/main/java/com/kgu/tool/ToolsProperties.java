package com.kgu.tool;

import com.kgu.sms.SmsProperties;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ConfigurationProperties(prefix = "kgu")
@PropertySource("classpath:tools.properties")
@Data
public class ToolsProperties {

    private SmsProperties sms;

}
