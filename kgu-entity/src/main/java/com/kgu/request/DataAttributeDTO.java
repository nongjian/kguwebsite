package com.kgu.request;

import com.kgu.entity.data.DataAttribute;
import com.kgu.entity.data.ModelAttribute;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Transient;

@Data
public class DataAttributeDTO {
    /*
     *目标Id
     */
    private String targetId;

    private String attributeId;

    private String type;

    private String value;

    private String name;

    public DataAttributeDTO(DataAttribute dataAttribute, String name){
        this.targetId= dataAttribute.getId();
        this.attributeId= dataAttribute.getAttributeId();
        this.type= dataAttribute.getType();
        this.value= dataAttribute.getValue();
        this.name=name;
    }
}
