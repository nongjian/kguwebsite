package com.kgu.request;

import com.kgu.common.PageRequest;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-03-10 16:26
 **/
@EqualsAndHashCode(callSuper = true)
@Data
public class QueryDataManagePageRequest extends PageRequest {

    @ApiModelProperty(value = "标题名称")
    private String title;

    @ApiModelProperty(value = "所属栏目id")
    private String sectionId;

    private String type;



}
