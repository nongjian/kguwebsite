package com.kgu.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Author shen
 * @Description 手机端定位功能
 * @Date 2021/6/25-15:05
 **/
@Data
public class Position {
    @ApiModelProperty(value = "经度")
    private  String  province;


}
