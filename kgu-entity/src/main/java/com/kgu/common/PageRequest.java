package com.kgu.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public abstract class PageRequest {

    @ApiModelProperty(value = "页码")
    protected Integer pageNum=1;

    @ApiModelProperty(value = "页面大小")
    protected Integer pageSize=10;
}
