package com.kgu.entity.leave;

import com.kgu.common.PageRequest;
import lombok.Data;

@Data
public class QueryLeaveRequest extends PageRequest {

    private String keyword;

    private String statue;

}
