package com.kgu.entity.leave;

import com.kgu.common.BaseEntity;
import io.swagger.annotations.Api;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "c_leave")
@Api("留言管理表")
@EqualsAndHashCode(callSuper = true)
public class Leave extends BaseEntity {

    @Column(columnDefinition = "varchar(10) comment '留言者姓名'")
    private String name;

    @Column(columnDefinition = "varchar(50) comment '公司'")
    private String corporation;

    @Column(columnDefinition = "varchar(20) comment '留言手机号'")
    private String phone;

    @Column(columnDefinition = "varchar(20) comment '留言邮箱'")
    private String email;

    @Column(columnDefinition = "varchar(100) comment '留言标题'")
    private String title;

    @Column(columnDefinition = "varchar(100) comment '留言栏目id'")
    private String sectionId;

    @Column(columnDefinition = "mediumtext comment '留言详情'")
    private String details;

    @Column(columnDefinition = "mediumtext comment '留言答复'")
    private String answer;

    @Column(columnDefinition = "varchar(10) comment '处理状态'")
    private String statue;

    @Column(name = "is_show",columnDefinition = "tinyint(1) comment '是否显示'")
    private Boolean show=Boolean.FALSE;
}
