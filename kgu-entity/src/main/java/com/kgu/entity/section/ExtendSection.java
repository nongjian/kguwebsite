package com.kgu.entity.section;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Table;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity(name = "c_extend_section")
@Table(appliesTo = "c_extend_section",comment = "拓展栏目管理")
public class ExtendSection implements Serializable {

    @Transient
    private static final long serialVersionUID= 7981560250804078637L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(columnDefinition="VARCHAR(32)")
    private String id;

    @Column(columnDefinition="varchar(40) comment '拓展的栏目id'")
    private String sectionId;

    @Column(columnDefinition="varchar(40) comment '数据id'")
    private String dataId;

    @Column(columnDefinition="text comment '拓展栏目的缩略图'")
    private String images;

}
