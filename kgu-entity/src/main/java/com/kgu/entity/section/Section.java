package com.kgu.entity.section;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kgu.common.BaseEntity;
import com.kgu.converter.Converter;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.tag.Tag;
import com.kgu.enums.SectionTypeEnum;
import com.kgu.enums.SysLanguage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.Table;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "c_section")
@Table(appliesTo = "c_section",comment = "应用中心-栏目管理")
public class Section extends BaseEntity {

    public Section() {
    }

    public Section(String template) {
        this.template = template;
    }

    @Column(columnDefinition="int(1) comment '多语言'")
    @ApiModelProperty(value = "多语言")
    private SysLanguage sysLanguage;

    private Integer indexNo;

    @Column(columnDefinition="varchar(255) comment '跳转链接'")
    @ApiModelProperty(value = "跳转链接")
    private String url;

    @OneToMany(mappedBy = "parentId",fetch = FetchType.EAGER)
    @OrderBy("comparable ASC")
    private List<Section> children=new LinkedList<>();

    @Column(columnDefinition="varchar(255) comment '栏目目录'")
    @ApiModelProperty(value = "栏目目录")
    private String name;

    @Column(columnDefinition="varchar(255) comment '网站模板'")
    @ApiModelProperty(value = "网站模板")
    private String template;

    @Column(columnDefinition="varchar(255) comment '网站标题'")
    @ApiModelProperty(value = "网站标题")
    private String title;

    @ApiModelProperty(value = "栏目背景图片")
    private String listImg;

    @ApiModelProperty(value = "图标图片")
    private String ico;


    @Column(columnDefinition="varchar(255) comment '描述'")
    @ApiModelProperty(value = "描述")
    private String description;

    @Column(columnDefinition="tinyint(1) comment '是否新窗口'")
    @ApiModelProperty(value = "新窗口")
    private Boolean blank;

    @Column(columnDefinition="varchar(255) comment '栏目分类'")
    @ApiModelProperty(value = "栏目分类")
    private SectionTypeEnum sectionType;

    @Column(columnDefinition="tinyint(1) comment '重定向url'")
    @ApiModelProperty(value = "重定向url")
    private String redirectUrl;

    @Column(columnDefinition="tinyint(1) comment 'seo图片'")
    @ApiModelProperty(value = "seo图片")
    private String seoImage;

    @Column(columnDefinition="tinyint(1) comment 'seo标题'")
    @ApiModelProperty(value = "seo关键词")
    private String seoTitle;

    @Column(columnDefinition="tinyint(1) comment 'seo关键词'")
    @ApiModelProperty(value = "seo关键词")
    private String keyword;

    @Column(columnDefinition="varchar(1) comment '导航位置'")
    @Convert(converter = Converter.class)
    @ApiModelProperty(value = "导航位置")
    private List<String> navPosition=new LinkedList<>();

    private String parentId;

    @ApiModelProperty(value = "标题图片")
    private String banner;

    @Transient
    private Map<String,Tag> sectionTags = new HashMap<>();

    @Transient
    private List<DataManage> datas = new LinkedList<>();

    @ApiModelProperty(value = "其他信息")
    @Column(columnDefinition="varchar(60) comment '其他信息'")
    private String columnOtherInfo;

    @Transient
    private String currentId;

    private String thumb;

    private Integer comparable;

    private String modelId;
}
