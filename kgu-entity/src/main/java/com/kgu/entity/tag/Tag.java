package com.kgu.entity.tag;


import com.kgu.common.BaseEntity;
import com.kgu.enums.SysLanguage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Date;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 16:00
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "c_tag")
@Table(appliesTo = "c_tag",comment = "应用中心-标签管理")
public class Tag extends BaseEntity {

    /**
     * 默认语言
     */
    private SysLanguage sysLanguage;

    /**
     * 标签名称
     */
    private String name;

    /**
     * ico
     */
    private String ico;

    /**
     * 跳转地址
     */
    private String redirectUrl;

    /**
     * 位置标识
     */
    private String locationIdentification;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 结束时间
     */
    private Date endTime;

    /**
     * 标签内容
     */
    @Column(columnDefinition = "text")
    private String content;


    /**
     * 关联站点
     */

    private String siteId;

    /**
     * 所属栏目
     */

    private String sectionId;

    @Column(name = "title",columnDefinition = "varchar(20)")
    private String title;

    @Column(name = "short_title",columnDefinition = "varchar(10)")
    private String shortTitle;

}
