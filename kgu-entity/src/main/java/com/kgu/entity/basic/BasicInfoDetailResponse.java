package com.kgu.entity.basic;

import com.kgu.common.BaseEntity;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 14:51
 **/
@Data
public class BasicInfoDetailResponse implements Serializable {
    @Transient
    private static final long serialVersionUID= 7981560250804078637L;

    @ApiModelProperty(value = "网站名称")
    private String name;

    @ApiModelProperty(value = "网址")
    private String url;

    @ApiModelProperty(value = "logo")
    private String pcLogo;

    @ApiModelProperty(value = "移动端logo")
    private String mobileLogo;


    @ApiModelProperty(value = "faviconLogo")
    private String faviconLogo;
    /**
     * 关键词
     */
    @ApiModelProperty(value = "关键词")
    private String keywords;
    /**
     * 描述
     */
    @ApiModelProperty(value = "描述")
    private String description;
    /**
     * 版权所有
     */
    @ApiModelProperty(value = "版权所有")
    private String copyright;
    /**
     * 底部其他信息
     */
    @ApiModelProperty(value = "底部其他信息")
    private String otherInfo;
    /**
     * 备案号
     */
    @ApiModelProperty(value = "备案号")
    private String icp;
    /**
     * 今日浏览量
     */
    @ApiModelProperty(value = "今日浏览量")
    private Integer pageViewToday;
    /**
     * 总浏览量
     */
    @ApiModelProperty(value = "总浏览量")
    private Integer pageViewTotal;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "地址")
    private String address;

    private String profile;
}
