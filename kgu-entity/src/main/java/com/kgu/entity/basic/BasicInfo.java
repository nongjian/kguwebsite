package com.kgu.entity.basic;

import com.kgu.common.BaseEntity;
import com.kgu.enums.SysLanguage;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;

import javax.persistence.Column;
import javax.persistence.Entity;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-21 13:31
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "c_basic_info")
@Table(appliesTo = "c_basic_info",comment = "应用中心-基本信息表")
public class BasicInfo extends BaseEntity {

    @ApiModelProperty(value = "网站名称")
    private String name;

    @ApiModelProperty(value = "网址")
    private String url;

    @ApiModelProperty(value = "logo")
    private String pcLogo;

    @ApiModelProperty(value = "移动端logo")
    private String mobileLogo;

    @ApiModelProperty(value = "faviconLogo")
    private String faviconLogo;
    /**
     * 关键词
     */
    private String keywords;

    /**
     * 描述
     */
    private String description;

    /**
     * 版权所有
     */
    private String copyright;

    /**
     * 底部其他信息
     */
    private String otherInfo;

    /**
     * 备案号
     */
    private String icp;

    /**
     * 今日浏览量
     */
    private Integer pageViewToday;

    /**
     * 总浏览量
     */
    private Integer pageViewTotal;

    /**
     * 关联站点
     */

    private String siteId;

    @Column(name = "phone", columnDefinition = "varchar(16)")
    private String phone;

    private String profile;

    @Column(name = "address", columnDefinition = "varchar(60)")
    private String address;

    @Column(name = "sys_language", columnDefinition = "tinyint(1)")
    private SysLanguage sysLanguage;
}
