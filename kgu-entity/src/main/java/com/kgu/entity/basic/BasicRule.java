package com.kgu.entity.basic;

import com.kgu.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "c_kgu_basic_rule")
@Table(appliesTo = "c_kgu_basic_rule",comment = "招聘信息")
public class BasicRule  extends BaseEntity {
    /**
     * 月度绩效
     */
    private String kpifrom;
    /**
     * 年度绩效
     */
    private String kpito;
    /**
     * 基础工资
     */
    private String basicSalary;
    /**
     * 提成点
     */
    private String commissionPoint;
    /**
     * 职位等级
     */
    private  Integer jobLevel;

    private  String jobTitle;
    /**
     * 提出比例
     */

    private  String proportion;


    private  String performanceRelatedPays;


    private  String personAccount;


    private  String embeddedCost;


    private  String costRate;


    private  String socialSecurity;


    private  String accumulationFund;


    private  String lengthOfSalary;


    private  String salary;


    private  String postSupplement;


    private  String fullAttendence;


    private  String mealAllowance;





}
