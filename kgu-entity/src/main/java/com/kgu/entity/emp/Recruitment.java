package com.kgu.entity.emp;

import com.kgu.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "emp_recruitment")
@Table(appliesTo = "emp_recruitment",comment = "招聘员工")
public class Recruitment extends BaseEntity {

    /**
     * 职位名称
     */
    private String positionName;

    /**
     * 薪资：
     */
    private String salary;

    /**
     * 工作城市：：
     */
    private String workCity;

    /**
     * 上班地址：：：
     */
    private String workAddress;

    /**
     * 工作年限
     */
    private String workingYearType;

    /**
     * 招聘人数
     */
    private String employeeNum;


    /**
     * 开始年龄
     */
    private String beginAge;
    /**
     * 结束年龄
     */
    private String endAge;

    /**
     * 学历
     */
    private String education;

    /**
     * 语言要求
     */
    private String languageType;

    /**
     * 熟练度
     */
    private String languageLevel;

    /**
     * 职位要求
     */
    private String demand;
    /**
     * 职位描述
     */
    private String description;

    /**
     * 站点id
     */
    private String siteId;


}
