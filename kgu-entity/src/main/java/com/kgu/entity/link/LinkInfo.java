package com.kgu.entity.link;

import com.kgu.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;
import org.springframework.beans.factory.annotation.Autowired;


import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "c_link_info")
@Table(appliesTo = "c_link_info",comment = "应用中心-栏目管理")
public class LinkInfo extends BaseEntity {


    @Column(columnDefinition="VARCHAR(20)")
    private String name;
    @Column(columnDefinition="VARCHAR(15)")
    private String phone;
    @Column(columnDefinition="VARCHAR(20)")
    private String corporation;
    @Column(columnDefinition="VARCHAR(500)")
    private String demand;
}
