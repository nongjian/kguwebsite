package com.kgu.entity.link;

import lombok.Data;

import javax.persistence.Column;
@Data
public class LinkInfoAddRequest {
    private String name;

    private String phone;

    private String corporation;

    private String demand;

    private String code;

    private String title;

    private String sectionId;
}
