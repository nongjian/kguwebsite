package com.kgu.entity.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.kgu.common.BaseEntity;
import com.kgu.enums.SysLanguage;
import com.kgu.request.DataAttributeDTO;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-03-10 13:26
 **/
@EqualsAndHashCode(callSuper = true)
@Data
@Entity(name = "c_data")
@Table(name = "c_data")
public class DataManage extends BaseEntity {

    private String jumpurl;
    /**
     * 多语言
     */
    @ApiModelProperty(value = "默认语言")
    private SysLanguage sysLanguage;

    /**
     * 跳转url
     */
    /**
     * 标题
     */
    private String title;

    /**
     * 简短标题
     */
    private String shortTitle;

    /**
     * 缩略图
     */
    private String image;
    /**
     * 二维码描述1
     */
    private  String qrCode;
    /**
     * 二维码描述2
     */
    private  String qrtwoCode;

    /**
     * 二维码1
     */
    @ApiModelProperty(value = "二维码1")
    private String high;
    /**
     * 二维码2
     */
    @ApiModelProperty(value = "二维码2")
    private String smallRoutine;


    /**
     * 支持评论
     */
    private Boolean comment;

    /**
     * 文件上传
     */
    private String file;

    /**
     * 头条
     */
    private Boolean head;

    /**
     * 最新
     */
    private Boolean newest;

    /**
     * 推荐
     */
    private Boolean recommend;

    /**
     * 热门
     */
    private Boolean hot;

    /**
     * 栏目目录
     */
    private String name;

    /**
     * 作者
     */
    private String author;

    /*开始时间*/
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date startTime;

    /**
     * 关键词
     */
    private String keyword;

    /**
     * 描述
     */
    private String description;

    /**
     * 内容
     */
    private String content;

    /*
     * 点击次数
     * */
    private Long count;

    private Integer hits;

    /**
     * 所属栏目
     */
    @org.hibernate.annotations.Index(name = "section_id")
    private String sectionId;

    @Column(name = "`type`", columnDefinition = "varchar(10)")
    private String type;

    private String seoname;
    /**
     * 关键词----> 标签
     */
    private String keywords;

    @Transient
    private Map<String,String> labelMap = new HashMap<>();

    @Transient
    private List<DataAttributeDTO> modelAttributes;
}
