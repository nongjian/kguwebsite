package com.kgu.entity.data;

import com.kgu.common.BaseEntity;
import com.kgu.enums.SysLanguage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;

import javax.persistence.Entity;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-25 14:25
 **/
@EqualsAndHashCode(callSuper = true,exclude = {})
@Data
@Entity(name = "c_model")
@Table(appliesTo = "c_model",comment = "数据管理-数据模型表")
public class DataModel extends BaseEntity {

    /**
     * 默认语言
     */
    private SysLanguage sysLanguage;

    /**
     * 模型名称
     */
    private String name;


    private String showName;

    /**
     * 备注
     */
    private String remark;

    /**
     * 排序
     */
    private Integer comparable;



    /**
     * 所属站点
     */
    private String siteId;
}
