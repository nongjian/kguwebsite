package com.kgu.entity.data;

import com.kgu.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Transient;

@EqualsAndHashCode
@Data
@Entity(name = "c_data_attribute")
@Table(appliesTo = "c_data_attribute",comment = "模型属性值-数据-关联表")
public class DataAttribute extends BaseEntity {

    /*
     *目标Id
     */
    @Column(columnDefinition = "varchar(36) comment 'targetId'")
    private String targetId;

    /*
     *属性Id
     */
    @Column(columnDefinition = "varchar(36) comment '模型属性id'")
    private String attributeId;

    /*
     *属性类型
     *默认为form
     */
    private String type;

    /*
     *属性值
     */
    @Column(columnDefinition = "varchar(1024) comment '属性值'")
    private String value;

    @Transient
    private String name;

}
