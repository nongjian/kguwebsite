package com.kgu.entity.data;

import com.kgu.common.BaseEntity;
import com.kgu.enums.AttributeInputTypeEnum;
import com.kgu.enums.AttributeOptionalEnum;
import com.kgu.enums.AttributeRetrieveEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Table;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true,exclude = {})
@Data
@Entity(name = "c_attribute")
@Table(appliesTo = "c_attribute",comment = "数据管理-模型属性表")
public class ModelAttribute extends BaseEntity {


    /**
     * 关联模型
     */
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "model_id")
    private String modelId;
    /**
     * 属性名称
     */
    private String name;

    private String sectionName;

    /**
     * 显示名称
     */
    private String showName;

    /**
     * 是否操作
     */
    private Boolean operating;

    /**
     * 能否进行检索
     */
    private AttributeRetrieveEnum retrieve;

    /**
     * 属性是否可选
     */
    private AttributeOptionalEnum optional;

    /**
     * 录入方式
     */
    private AttributeInputTypeEnum inputType;

    /**
     * 可选值列表
     */
    private String optionalValues;

    /**
     * 是否必填
     */
    private Boolean required;

    /**
     * 提示信息
     */
    private String notice;

    /**
     * 排序
     */
    private Integer comparable;

}
