package com.kgu.enums;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-18 14:22
 **/
public enum SysLanguage {
    ENG,
    ZH_CN
}
