package com.kgu.enums;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-25 14:32
 **/
public enum  AttributeRetrieveEnum {


    NO_RETRIEVAL_REQUIRED, //不需要检索
    KEYWORD_RETRIEVE, //关键字检索
    RANGE_RETRIEVE //范围检索
}
