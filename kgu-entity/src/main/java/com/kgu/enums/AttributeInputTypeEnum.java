package com.kgu.enums;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-25 14:55
 **/
public enum AttributeInputTypeEnum {

    MANUAL_INPUT, //手工录入
    MULTILINE_TEXT_BOX, //多行文本框
    FILE_UPLOAD, //文件上传
    SINGLE_IMAGE_UPLOAD, //单图上传
    MULTI_IMAGE_UPLOAD, //多图上传
    SELECT_FROM_THE_LIST_BELOW, //从下面的列表中选择
    EDITOR,//编辑器
    LINKED_DATA,//关联数据
    MULTIPLE_FIELDS,//多字段

}
