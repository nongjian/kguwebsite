package com.kgu.enums;

/**
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 *
 * @author cnyuchu@gmail.com
 * @create 2020-02-25 14:34
 **/
public enum AttributeOptionalEnum {

    UNIQUE, //唯一属性
    RADIO, //单选属性
    CHECK,  //复选属性
    SELECT, //下拉菜单
}
