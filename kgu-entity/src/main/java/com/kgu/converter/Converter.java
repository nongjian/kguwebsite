package com.kgu.converter;

import org.springframework.stereotype.Component;

import javax.persistence.AttributeConverter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

@javax.persistence.Converter
@Component
public class Converter implements AttributeConverter<List<String>,String> {
    @Override
    public String convertToDatabaseColumn(List<String> attribute) {
        return attribute != null ? String.join(",", attribute) : null;
    }

    @Override
    public List<String> convertToEntityAttribute(String dbData) {
        return dbData != null ? new ArrayList<>(Arrays.asList(dbData.split(","))) : new LinkedList<>();
    }
}
