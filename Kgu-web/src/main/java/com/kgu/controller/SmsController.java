package com.kgu.controller;

import com.kgu.cache.ICacheService;
import com.kgu.cache.impl.CacheServiceImpl;
import com.kgu.common.Result;
import com.kgu.sms.ISmsService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@RestController()
@RequestMapping("")
public class SmsController {

    @Autowired
    private ISmsService smsService;

    @Autowired
    private ICacheService iCacheService;


    @RequestMapping("/sms")
    public Result<Boolean> sms(@RequestParam("phone") String phone){
        String code = RandomStringUtils.randomNumeric(6);
        iCacheService.setClientCache(CacheServiceImpl.VALIDATE_CODE + phone, code, 5, TimeUnit.MINUTES);  //5分钟有效期
        Boolean re = true;
        try {
            re = smsService.send(phone,code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Result.Builder<Boolean>().data(re).build();
    }

}
