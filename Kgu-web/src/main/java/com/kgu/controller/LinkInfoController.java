package com.kgu.controller;

import com.kgu.cache.ICacheService;
import com.kgu.cache.impl.CacheServiceImpl;
import com.kgu.common.Result;
import com.kgu.data.IDataManageService;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.leave.Leave;
import com.kgu.entity.link.LinkInfo;
import com.kgu.entity.link.LinkInfoAddRequest;

import com.kgu.leave.LeaveRepository;
import com.kgu.link.ILinkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/link")
public class LinkInfoController {

    @Autowired
    IDataManageService iDataManageService;

    @Autowired
    private ICacheService iCacheService;

    @Autowired
    private LeaveRepository leaveRepository;

    @Autowired
    private JavaMailSender mailSender; //发送邮件

    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    @PostMapping("/add")
    public Result<String> add(@RequestBody LinkInfoAddRequest linkInfoAddRequest) {
        if (!linkInfoAddRequest.getCode().equals(iCacheService.getClientCache(CacheServiceImpl.VALIDATE_CODE + linkInfoAddRequest.getPhone()))) {
            return new Result.Builder<String>().code(400).data("验证码错误").build();
        }
        Leave leave = new Leave();
        leave.setSectionId(linkInfoAddRequest.getSectionId());
        leave.setCorporation(linkInfoAddRequest.getCorporation());
        leave.setName(linkInfoAddRequest.getName());
        leave.setPhone(linkInfoAddRequest.getPhone());
        leave.setDetails(linkInfoAddRequest.getDemand());
        if (linkInfoAddRequest.getDemand() != null && linkInfoAddRequest.getDemand().length() > 0) {
            sendMail(linkInfoAddRequest.getDemand());
        }
        leave.setDeleted(false);
        leave.setAvailable(true);
        leave.setShow(true);
        leave.setStatue("1");
        leave.setTitle("官网留言");
        Leave re = leaveRepository.save(leave);
        return new Result.Builder<String>().data("SUCCESS").build();
    }

    /**
     * 发送邮件 -> 可发送多人
     *
     * @param
     * @return
     * @author shen
     **/
    public void sendMail(String content) {


        MimeMessage mimeMessage = mailSender.createMimeMessage();
        List<String> emailList = new LinkedList<>();
        emailList.add("sophie@kgu.cn");
        emailList.add("super@kgu.cn");
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            /*helper.setTo(to);*/
            helper.setSubject("官网申请要求");
            helper.setText(content);
            helper.setFrom("sophie@kgu.cn");
            //发送给多人 这里是我的实体类 可以改为对应的邮箱集合
            for (String email : emailList) {
                mimeMessage.addRecipients(Message.RecipientType.TO, InternetAddress.parse(email, false));
            }
            mailSender.send(mimeMessage);
            logger.info("官网申请要求邮件已发送。");
        } catch (MessagingException e) {
            logger.error("官网申请要求邮件时发生异常了！", e);
        }


//        SimpleMailMessage message = new SimpleMailMessage();
//        message.setFrom("sophie@kgu.cn");
//        message.setTo("18877828915@163.com");
//        message.setTo("767021661@qq.com");
//        message.setSubject("官网申请要求");
//        message.setText(content);
//
//        try {
//
//            mailSender.send(message);
//
//            logger.info("官网申请要求邮件已发送。");
//
//        } catch (Exception e) {
//
//            logger.error("官网申请要求邮件时发生异常了！", e);
//
//        }
    }

}
