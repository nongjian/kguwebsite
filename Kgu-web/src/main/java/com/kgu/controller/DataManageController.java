package com.kgu.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.kgu.data.IDataManageService;
import org.hibernate.query.internal.NativeQueryImpl;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("")
public class DataManageController {

    @Autowired
    private IDataManageService dataManageService;

    @Autowired
    EntityManager entityManager;

    @RequestMapping("/getSeachList")
    public String getSeachList(@RequestParam("title") String title){
        Query query = entityManager.createNativeQuery("select cd.title as `value` from c_data as cd where cd.title like concat('%','"+title+"','%') and cd.deleted=0");
        query.unwrap(NativeQueryImpl.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
        List<Map> list = query.getResultList();
        JSONObject jsonObject = new JSONObject();
        return JSON.toJSONString(list, SerializerFeature.WriteDateUseDateFormat);
    }

}
