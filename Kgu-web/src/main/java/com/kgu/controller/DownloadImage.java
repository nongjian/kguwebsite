//package com.kgu.controller;
//
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import javax.servlet.http.HttpServletResponse;
//import java.io.File;
//import java.io.FileInputStream;
//import java.io.IOException;
//import java.io.OutputStream;
//
//@RestController
//@RequestMapping("/file")
//public class DownloadImage {
//
//    @GetMapping("/{site}/{time}/{filename}")
//    public void images(@PathVariable("site") String site, @PathVariable("time") String time,
//                       @PathVariable("filename") String filename, HttpServletResponse response) throws IOException {
//        File file=new File("resources/upload/image/"+site+"/"+time+"/"+filename);
//        FileInputStream inputStream=new FileInputStream(file);
//        byte[] data=new byte[(int)file.length()];
//        int lenght=inputStream.read(data);
//        inputStream.close();
//        response.setContentType("application/octet-stream");
//        OutputStream stream = response.getOutputStream();
//        stream.write(data);
//        stream.flush();
//        stream.close();
//    }
//}
