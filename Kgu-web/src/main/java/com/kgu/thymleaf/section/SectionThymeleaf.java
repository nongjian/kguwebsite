package com.kgu.thymleaf.section;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.kgu.ContextVariable;
import com.kgu.SectionRepository;
import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.Thymeleaf.section.SectionThymeleafStrategy;
import com.kgu.Utils;
import com.kgu.common.Position;
import com.kgu.data.IDataManageService;
import com.kgu.emp.IRecruitmentService;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.emp.Recruitment;
import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;
import com.kgu.section.SectionService;
import com.kgu.tag.ITagService;
import com.vt.aop.URLRewriter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.context.WebContext;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("")
public class SectionThymeleaf {
//
//    @GetMapping(value = {"/resources/resources/upload/image/{a}/{b}/{c}","/file/{a}/{b}/{c}","/file/{a}/{b}"})
//    public String aaa(@PathVariable String a, @PathVariable String b, @PathVariable(value = "c", required = false) String c){
//        return null;
//    }

    @Autowired
    private SectionRepository sectionRepository;
    @Autowired
    private SpringTemplateEngine templateEngine;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private IDataManageService iDataManageService;

    @Autowired
    private ITagService iTagService;

    @Autowired
    private IRecruitmentService recruitmentService;

    @Autowired
    private SectionThymeleafStrategy sectionThymeleafStrategy;

    @Autowired
    RestTemplate restTemplate;


    @RequestMapping(value = {"/{sectionName}", "/{parentName}/{sectionName}"})
//    @URLRewriter(section = "section")
    public String section(Model model,
                          HttpServletRequest httpServletRequest,
                          @PathVariable(value = "sectionName") String sectionName,
                          @PathVariable(value = "parentName", required = false) String parentName,
                          @RequestParam(value = "pageNum", defaultValue = "0", required = false) Integer pageNum,
                          @RequestParam(value = "pageSize", defaultValue = "9", required = false) Integer pageSize,
                          @RequestParam(value = "query", defaultValue = "", required = false) String query,
                          @RequestParam(value = "title", defaultValue = "", required = false) String title) {

        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
//        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        List<Section> allSection = sectionService.findAll();
        List<Tag> list = iTagService.findAll();
        ContextVariable.set(parameterMap);
        ContextVariable.setAllSection(allSection);
        ContextVariable.setAllTag(list);
        Map<String, List<Tag>> map = list.stream().collect(Collectors.groupingBy(Tag::getSectionId));
        allSection.forEach(section ->{
            if (map.get(section.getId())!=null) {
                section.setSectionTags(map.get(section.getId())
                        .stream()
                        .collect(Collectors.toMap(Tag::getName, a -> a, (a1, a2) -> a2)));
            }
        });
        List<Section> sectionList = allSection.stream().filter(s->{return s.getParentId()==null;}).collect(Collectors.toList());
        sectionList.forEach(x->{x.setComparable(x.getComparable()!=null?x.getComparable():0);});
        sectionList = sectionList.stream().sorted(Comparator.comparing(Section::getComparable).reversed()).collect(Collectors.toList());
        Optional<Section> optional = allSection.stream().filter(s->{return sectionName.replace(".html","").equals(s.getName());}).findFirst();
        Map<String,Section> all = allSection.stream().collect(Collectors.toMap(Section::getId,a1->a1,(a1,a2)->a2));
        Section sectionPage = optional.orElse(new Section("error"));
        sectionPage.setCurrentId(sectionPage.getId());
        if (StringUtils.isNotBlank(parentName)){
            Section sectionParent=sectionRepository.findOneByName(parentName.replace(".html",""));
            sectionPage.setCurrentId(sectionParent.getId());
        }
        model.addAttribute("allSec",all);
        SectionThymeleafService sectionThymeleafService=sectionThymeleafStrategy.sectionThymeleaf(sectionPage.getTemplate());
        if (sectionThymeleafService==null){
            sectionThymeleafService = sectionThymeleafStrategy.sectionThymeleaf("error");
        }

        String sectionThymeleafService1 = sectionThymeleafService
                .setParameter(pageNum, pageSize)
                .setQueryParameter(query)
                .setCaseQueryTitle(title)
                .section(model, sectionList, sectionPage);
        return sectionThymeleafService1;

    }


    @RequestMapping(value = {"/seacher/{id}"})
    @ResponseBody
    public String seacher(@PathVariable("id") String id) {
        List<Section> sectionList = sectionRepository.findAllByParentIdIsNullAndAvailableIsTrue();
        DataManage dataManage = iDataManageService.searchData(id);
        if (dataManage==null) {
            return "";
        }
        Optional<Section> optionalSection = sectionRepository.findById(dataManage.getSectionId());

        if ("news".equals(optionalSection.get().getTemplate())) {
            return "news/"+optionalSection.get().getTemplate()+"/news/"+dataManage.getId()+"";
        } else if ("case".equals(optionalSection.get().getTemplate())){
            return "detail/"+optionalSection.get().getTemplate()+"/case/"+dataManage.getId()+"";
        }
        return "";
    }

    /**
     * @return
     * @Author tyler
     * @Description //TODO 新闻和案例详情跳转
     * @Date
     * @Param
     **/
    @RequestMapping(value = {"/case/{id}"})
    public String casedetail(Model model, @PathVariable("id") String id) {
        List<Section> allSection = sectionService.findAll();
        List<Section> sectionList = allSection.stream().filter(s->{return s.getParentId()==null;}).collect(Collectors.toList());
        sectionList.forEach(x->{x.setComparable(x.getComparable()!=null?x.getComparable():0);});
        sectionList = sectionList.stream().sorted(Comparator.comparing(Section::getComparable).reversed()).collect(Collectors.toList());
        Optional<DataManage> optional = iDataManageService.findbyseoname(id);
        Optional<DataManage> optional1 = optional.isPresent()?optional:iDataManageService.findOneById(id);
        Optional<Section> sectionPage = sectionRepository.findById(optional1.get().getSectionId());
        return sectionThymeleafStrategy.
                sectionThymeleaf("detail")
                .setParameter(optional1.get().getId(),"")
                .section(model, sectionList, sectionPage.get());
    }

    /**
     * @return
     * @Author tyler
     * @Description //TODO 新闻和案例详情跳转
     * @Date
     * @Param
     **/
    @RequestMapping(value = {"/case.html/{id}"})
    public String casedetailhtml(Model model, @PathVariable("id") String id) {
        List<Section> allSection = sectionService.findAll();
        List<Section> sectionList = allSection.stream().filter(s->{return s.getParentId()==null;}).collect(Collectors.toList());
        sectionList.forEach(x->{x.setComparable(x.getComparable()!=null?x.getComparable():0);});
        sectionList = sectionList.stream().sorted(Comparator.comparing(Section::getComparable).reversed()).collect(Collectors.toList());
        Optional<DataManage> optional = iDataManageService.findbyseoname(id);
        Optional<DataManage> optional1 = optional.isPresent()?optional:iDataManageService.findOneById(id);
        Optional<Section> sectionPage = sectionRepository.findById(optional1.get().getSectionId());
        return sectionThymeleafStrategy.
                sectionThymeleaf("detail")
                .setParameter(optional1.get().getId(),"")
                .section(model, sectionList, sectionPage.get());
    }


    /**
     * 新闻页详情
     * @param model
     * @param id
     * @return
     */
    @RequestMapping(value = {"/news/{id}"})
    public String newsdetail(Model model,
                             @PathVariable("id") String id
    ) {
        List<Section> allSection = sectionService.findAll();
        List<Section> sectionList = allSection.stream().filter(s->{return s.getParentId()==null;}).collect(Collectors.toList());
        sectionList.forEach(x->{x.setComparable(x.getComparable()!=null?x.getComparable():0);});
        sectionList = sectionList.stream().sorted(Comparator.comparing(Section::getComparable).reversed()).collect(Collectors.toList());
        Optional<DataManage> optional = iDataManageService.findOneById(id);
        DataManage dataManage = optional.get();
        if (dataManage.getHits() == null){
            dataManage.setHits(1000);
        }
        dataManage.setHits(dataManage.getHits()+1);
        iDataManageService.save(dataManage);
        Optional<Section> sectionPage = sectionRepository.findById(optional.get().getSectionId());
        iDataManageService.preAndNextDataManage(model,id);
        recommendList(model, sectionPage.get());
        return sectionThymeleafStrategy.sectionThymeleaf("detail").setParameter(id, "news").section(model, sectionList, sectionPage.get());
    }

    /**
     * 招聘页动态展  --- 手机端定位功能
     * @return
     */

    @PostMapping(value = {"/recruit/address/recruitment"})
    @ResponseBody
    public String  jobList(
            Model model,
            @RequestParam("query")  String query
    ) {
        List<Recruitment> workCityList = recruitmentService.getWorkCityList();
        Recruitment recruitment = new Recruitment();
        boolean flag = false;
        for (int i = 0; i < workCityList.size(); i++) {
            if (query.contains(workCityList.get(i).getWorkCity())) {
                recruitment = workCityList.get(i);
                flag = true;
            }
        }
        if (!flag) {
            recruitment = recruitmentService.findOneById("4");
        }
        String siteId = recruitment.getSiteId();
        phoneList(model, recruitment.getWorkCity());
        return siteId;
    }
    /**
     * 手机端/电脑端 首次获取定位对应数据
     *
     * @param model,position
     * @author shen
     * @return org.springframework.ui.Model
     **/
    public Model phoneList(Model model, String position){

        //获取手机端 城市列表
        List<Recruitment> workCityList = recruitmentService.getWorkCityList();
        model.addAttribute("phoneCityList",workCityList);
        model.addAttribute("ajaxPhoneCityList",workCityList);
        //获取手机端 工作列表
        List<Recruitment>  phoneList = recruitmentService.phoneList(position);
        model.addAttribute("phoneList",phoneList);
        //电脑端 工作列表
        model.addAttribute("recruitments",phoneList);
        return  model;
    }




    /**
     * 推荐新闻
     */

    public  Model recommendList(Model model,Section sectionPage){

        List<DataManage> dataManages = iDataManageService.recommendNews(sectionPage.getId());
        List<DataManage> collect = dataManages.stream().limit(10).collect(Collectors.toList());
        model.addAttribute("collectList",collect);
        return  model;
    }
}
