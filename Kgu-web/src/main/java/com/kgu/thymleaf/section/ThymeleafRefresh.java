package com.kgu.thymleaf.section;

import com.kgu.ContextVariable;
import com.kgu.PageUtil;
import com.kgu.SectionRepository;
import com.kgu.Thymeleaf.section.SectionThymeleafService;
import com.kgu.Thymeleaf.section.SectionThymeleafStrategy;
import com.kgu.Utils;
import com.kgu.common.Result;
import com.kgu.data.IDataManageService;
import com.kgu.emp.IRecruitmentService;
import com.kgu.entity.data.DataManage;
import com.kgu.entity.emp.Recruitment;
import com.kgu.entity.section.ExtendSection;
import com.kgu.entity.section.Section;
import com.kgu.entity.tag.Tag;
import com.kgu.request.QueryDataManagePageRequest;
import com.kgu.section.SectionService;
import com.kgu.tag.ITagService;
import com.vt.aop.URLRewriter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.thymeleaf.context.WebContext;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller
@RequestMapping("")
public class ThymeleafRefresh {
    @Autowired
    private SectionThymeleafStrategy sectionThymeleafStrategy;
    @Autowired
    IRecruitmentService  recruitmentService;

    @Autowired
    private SectionRepository sectionRepository;
    @Autowired
    private ITagService iTagService;
    @Autowired
    private IDataManageService iDataManageService;
    @Autowired
    IRecruitmentService iRecruitmentService;

    @Autowired
    private SectionService sectionService;

    @Autowired
    private EntityManager entityManager;


    @RequestMapping("/refresh/{sectionName}")
    public String queryCase(Model model,
                            @PathVariable("sectionName") String sectionName,
                            @RequestParam(value = "title", defaultValue = "", required = false) String title,
                            @RequestParam(value = "query", defaultValue = "", required = false) String query,
                            @RequestParam(value = "pageNum", defaultValue = "0", required = false) Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "9", required = false) Integer pageSize,
                            @RequestParam(value = "flag", defaultValue = "", required = false) String flag) {

        List<Section> allSection = sectionService.findAll();
        Section sectionPage = sectionRepository.findOneByName(sectionName);
        QueryDataManagePageRequest queryDataManagePageRequest = new QueryDataManagePageRequest();
        queryDataManagePageRequest.setSectionId(sectionPage.getId());
        queryDataManagePageRequest.setTitle(title);
        queryDataManagePageRequest.setType(query);
        queryDataManagePageRequest.setPageNum(pageNum);
        queryDataManagePageRequest.setPageSize(pageSize);
        Result<Page<DataManage>> pageResult = iDataManageService.findCaseList(sectionPage.getId(), queryDataManagePageRequest);
        model.addAttribute("sectionPage", sectionPage);
        model.addAttribute("caseList", pageResult);
        model.addAttribute("phoneList", Utils.label(allSection,pageResult.getData().getContent()));
        List<Integer> list1 = PageUtil.pageInfo(pageNum,pageResult.getData().getTotalPages());
        model.addAttribute("page",list1);
        return "/common/data::"+flag+"caseList_content";
    }

    /**
     * 新闻页的局部刷新
     *
     * @param model
     * @param sectionName
     * @param title
     * @param pageNum
     * @param pageSize
     * @return
     */
    @RequestMapping("/list/{sectionName}")
    public String queryNews(Model model,
                            @PathVariable("sectionName") String sectionName,
                            @RequestParam(value = "title", defaultValue = "", required = false) String title,
                            @RequestParam(value = "pageNum", defaultValue = "0", required = false) Integer pageNum,
                            @RequestParam(value = "pageSize", defaultValue = "12", required = false) Integer pageSize) {
        Section sectionPage = sectionRepository.findOneByTemplate(sectionName);
        QueryDataManagePageRequest query = new QueryDataManagePageRequest();
        query.setSectionId(sectionPage.getId());
        query.setTitle(title);
        query.setPageNum(pageNum);
        query.setPageSize(pageSize);
        Result<Page<DataManage>> pageResult = iDataManageService.findAllBySectionId(sectionPage.getId(), query);
        List<DataManage> content = pageResult.getData().getContent();
        List<String> pageNewsIds = content.stream().map(dataManage -> dataManage.getId()).collect(Collectors.toList());
        model.addAttribute("newsIds", pageNewsIds);
        model.addAttribute("sectionPage", sectionPage);
        model.addAttribute("newsList", pageResult);
        List<Integer> list1 = PageUtil.pageInfo(pageNum,pageResult.getData().getTotalPages());
        model.addAttribute("page",list1);
        return "/common/data1::newsList_content";
    }

    /**
     * @param model
     * @param sectionName
     * @param pageNum
     * @param pageSize
     * @param flag        用于区别手机端刷新还是pc端刷新
     * @return
     */
    @RequestMapping("/load/{sectionName}")
    public String load(Model model,
                       @PathVariable("sectionName") String sectionName,
                       @RequestParam(value = "pageNum", defaultValue = "0", required = false) Integer pageNum,
                       @RequestParam(value = "pageSize", defaultValue = "9", required = false) Integer pageSize,
                       @RequestParam(value = "flag", defaultValue = "", required = false) String flag
    ) {
        List<Section> allSection = sectionService.findAll();
        Map<String,Section> all = allSection.stream().collect(Collectors.toMap(Section::getId,a1->a1,(a1,a2)->a2));
        model.addAttribute("allSec",all);
        Section sectionPage = allSection.stream().filter(s -> {
            return s.getName().equals(sectionName);
        }).findFirst().get();
        List<DataManage> list = iDataManageService.findBySectionIdAndExpendSection(sectionPage.getId(), (pageNum + 1) * pageSize);
        List<DataManage> filterList = list.stream().filter(data->{return !data.getSectionId().equals("2c9180887945b521017945c40610008a");}).collect(Collectors.toList());
        model.addAttribute("dataList", filterList);
        return "/common/" + sectionPage.getTemplate() + "::" + sectionPage.getTemplate() + flag + "";
    }

    /**
     * 招聘信息的局部刷新  -> 电脑端
     *
     * @param model
     * @param sectionName
     * @return
     */
    @RequestMapping("/job/{sectionName}/{city}/{position}")
    public String jobContent(Model model,
                             @PathVariable("sectionName") String sectionName,
                             @PathVariable("city") String city,
                             @PathVariable("position") String position) {
        List<Recruitment> workCityList = recruitmentService.getWorkCityList();
        if (city.equals("0")){
            city = "上海";
        }
        List<Recruitment> positionNameList = recruitmentService.getPositionNameList(city);
        boolean flag = false;
        for (Recruitment recruitment : positionNameList) {
            if (position.equals(recruitment.getPositionName())){
                flag = true;
            }
        }
        if (!flag){
            position = positionNameList.get(0).getPositionName();
        }
        String [] parameter = {city,position};
        List<Recruitment> recruitments = recruitmentService.findAllByWorkCityAndPositionNameAndDeletedIsFalse(city,position);
        model.addAttribute("parameter",parameter);
        model.addAttribute("recruitments",recruitments);
        model.addAttribute("workCityList",workCityList);
        model.addAttribute("positionNameList",positionNameList);


        return "/common/recruitment::job_content";
    }



    /**
     * 招聘信息的局部刷新
     *
     * @param model
     * @return
     */
    @GetMapping("/job/recruitment/phone/{city}")
    public String jobPhoneContent(Model model,
                             @PathVariable("city") String city
                             ) {
        List<Recruitment> workCityList = recruitmentService.getWorkCityList();
        if (city.equals("0")){
            city = "上海";
        }
        List<Recruitment>  phoneList = recruitmentService.phoneList(city);
        List<Recruitment> positionNameList = recruitmentService.getPositionNameList(city);
        model.addAttribute("phoneCityList",workCityList);
        model.addAttribute("phoneList",phoneList);
        model.addAttribute("recruitments",phoneList);
        model.addAttribute("workCityList",workCityList);
        model.addAttribute("positionNameList",positionNameList);


        return "/common/recruitment::job_content";
    }




    @RequestMapping("/image")
    public String image(Model model,
                        @RequestParam("dataId") String dataId,
                        @RequestParam("sectionId") String sectionId){
        Query query = entityManager.createQuery("select ces.images from c_extend_section as ces where ces.dataId='"+dataId+"' and ces.sectionId='"+sectionId+"'");
        Object extendSection = query.getSingleResult();
        Map<String,String> map = new HashMap<>();
        map.put("image",extendSection.toString());
        model.addAttribute("detail",map);
        return "/common/casedetail::img";
    }

}
