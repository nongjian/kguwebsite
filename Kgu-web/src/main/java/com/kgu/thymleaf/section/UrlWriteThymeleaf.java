//package com.kgu.thymleaf.section;
//
//import com.kgu.ContextVariable;
//import com.kgu.SectionRepository;
//import com.kgu.Thymeleaf.section.SectionThymeleafService;
//import com.kgu.Thymeleaf.section.SectionThymeleafStrategy;
//import com.kgu.common.Result;
//import com.kgu.data.IDataManageService;
//import com.kgu.emp.IRecruitmentService;
//import com.kgu.entity.section.Section;
//import com.kgu.entity.tag.Tag;
//import com.kgu.section.SectionService;
//import com.kgu.tag.ITagService;
//import com.vt.aop.URLRewriter;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
//import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.client.RestTemplate;
//import org.springframework.web.context.request.RequestContextHolder;
//import org.springframework.web.context.request.ServletRequestAttributes;
//import org.thymeleaf.context.WebContext;
//import org.thymeleaf.spring5.SpringTemplateEngine;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.util.List;
//import java.util.Map;
//import java.util.Optional;
//import java.util.stream.Collectors;
//
//@RequestMapping("")
//@RestController
//public class UrlWriteThymeleaf {
//    @Autowired
//    private SectionThymeleafStrategy sectionThymeleafStrategy;
//    @Autowired
//    private SpringTemplateEngine templateEngine;
//    @Autowired
//    private SectionRepository sectionRepository;
//    @Autowired
//    private SectionService sectionService;
//    @Autowired
//    private IDataManageService iDataManageService;
//    @Autowired
//    private ITagService iTagService;
//    @Autowired
//    private IRecruitmentService recruitmentService;
//
//    @Autowired
//    RestTemplate restTemplate;
//
//    @RequestMapping(value = {"getsections"})
//    public Result section(){
//        List<Section> allSection = sectionService.findAll();
//    }
//
////    @RequestMapping(value = {"/{sectionName}", "/{parentName}/{sectionName}"})
//////    @URLRewriter(section = "section")
////    public String section(Model model,
////                          HttpServletRequest httpServletRequest,
////                          @PathVariable(value = "sectionName") String sectionName,
////                          @PathVariable(value = "parentName", required = false) String parentName,
////                          @RequestParam(value = "pageNum", defaultValue = "0", required = false) Integer pageNum,
////                          @RequestParam(value = "pageSize", defaultValue = "9", required = false) Integer pageSize,
////                          @RequestParam(value = "query", defaultValue = "", required = false) String query,
////                          @RequestParam(value = "title", defaultValue = "", required = false) String title) {
////        Map<String, String[]> parameterMap = httpServletRequest.getParameterMap();
////        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
////        List<Section> allSection = sectionService.findAll();
////        List<Tag> list = iTagService.findAll();
////        ContextVariable.set(parameterMap);
////        ContextVariable.setAllSection(allSection);
////        ContextVariable.setAllTag(list);
////        Map<String, List<Tag>> map = list.stream().collect(Collectors.groupingBy(Tag::getSectionId));
////        allSection.forEach(section ->{
////            if (map.get(section.getId())!=null) {
////                section.setSectionTags(map.get(section.getId())
////                        .stream()
////                        .collect(Collectors.toMap(Tag::getName, a -> a, (a1, a2) -> a2)));
////            }
////        });
////        List<Section> sectionList = allSection.stream().filter(s->{return s.getParentId()==null;}).collect(Collectors.toList());
////        Optional<Section> optional = allSection.stream().filter(s->{return sectionName.replace(".html","").equals(s.getName());}).findFirst();
////        Map<String,Section> all = allSection.stream().collect(Collectors.toMap(Section::getId,a1->a1,(a1,a2)->a2));
////        Section sectionPage = optional.orElse(new Section("error"));
////        sectionPage.setCurrentId(sectionPage.getId());
////        if (StringUtils.isNotBlank(parentName)){
////            Section sectionParent=sectionRepository.findOneByName(parentName.replace(".html",""));
////            sectionPage.setCurrentId(sectionParent.getId());
////        }
////        model.addAttribute("allSec",all);
////        SectionThymeleafService sectionThymeleafService=sectionThymeleafStrategy.sectionThymeleaf(sectionPage.getTemplate());
////        if (sectionThymeleafService==null){
////            sectionThymeleafService = sectionThymeleafStrategy.sectionThymeleaf("error");
////        }
////        String sectionThymeleafService1 = sectionThymeleafService
////                .setParameter(pageNum, pageSize)
////                .setQueryParameter(query)
////                .setCaseQueryTitle(title)
////                .section(model, sectionList, sectionPage);
////        return sectionThymeleafService1;
//////        return templateEngine.process(sectionThymeleafService1,new WebContext(httpServletRequest, response, httpServletRequest.getServletContext(),
//////                httpServletRequest.getLocale(), model.asMap()));
////    }
//
//
//}
