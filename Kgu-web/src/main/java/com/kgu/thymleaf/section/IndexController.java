package com.kgu.thymleaf.section;

import com.kgu.SectionRepository;
import com.kgu.entity.section.Section;
import com.kgu.section.SectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/*")
public class IndexController {

    @Autowired
    private SectionRepository sectionRepository;

    @RequestMapping("")
    public void index(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        try {
            Section section = sectionRepository.findAllByTemplate("home");
            try {
                httpServletRequest.getRequestDispatcher(section.getName()).forward(httpServletRequest,httpServletResponse);
            } catch (ServletException e) {
                System.out.println(e);
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println(e);
            e.printStackTrace();
        }
    }

}
