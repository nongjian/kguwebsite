function setPage(nowpage,totalCount){     //nowpage:当前页数  totalCount：数据总条数
	let pageSize =Math.ceil(totalCount/10);    //总页数
	let showSize = 5;  //显示的页数
	let maxPage=pageSize;  
	let oldPage=1;
	if(showSize<=pageSize){
		maxPage=showSize;
	}
	if(nowpage>pageSize){
		nowpage=pageSize;
	}
	if(nowpage<1){
		nowpage=1;
	}
	$("#page").append('<ul><li style="width:40px;">首页</li><li>&laquo;</li>');
			if(nowpage>Math.ceil(showSize/2)){
				if(nowpage<=pageSize-2&&nowpage>=3){
					for(let i=nowpage-2;i<=eval(nowpage+"+2");i++){
						if(i==nowpage){
							$("#page ul").append('<li class="active">'+i+'</li>');
						}
						else{
							$("#page ul").append('<li>'+i+'</li>');
						}
					}
				}
				else if(nowpage==pageSize-1){
					for(let i=nowpage-3;i<=eval(nowpage+"+1");i++){
						if(i==nowpage){
							$("#page ul").append('<li class="active">'+i+'</li>');
						}
						else{
							$("#page ul").append('<li>'+i+'</li>');
						}
					}
				}
				else{
					for(let i=nowpage-4;i<=nowpage;i++){
						if(i==nowpage){
							$("#page ul").append('<li class="active">'+i+'</li>');
						}
						else{
							$("#page ul").append('<li>'+i+'</li>');
						}
					}
				}
			}
			else{
				
					for(let i=1;i<=showSize;i++){
						if(i==nowpage){
							$("#page ul").append('<li class="active">'+i+'</li>');
						}
						else{
							$("#page ul").append('<li>'+i+'</li>');
						}
					}
			}
			oldPage=nowpage;
	$("#page ul").append('<li>&raquo;</li><li style="width:40px;">末页</li></ul>')
	$("#page ul li").click(function(){
		nowpage= $(this).text();
		if($(this).text()=="首页"){
			$("#page").empty()
			setPage(1,totalCount);
			$(this).siblings("li").removeClass("active");
		}
		else if($(this).text()=="末页"){
			$("#page").empty()
			setPage(pageSize,totalCount);
			$(this).siblings("li").removeClass("active");
		}
		else if($(this).text()=="«"){
			$("#page").empty()
			setPage(Math.ceil(oldPage-1),totalCount);
			$(this).siblings("li").removeClass("active");
		}
		else if($(this).text()=="»"){
			$("#page").empty()
			setPage(Math.ceil(oldPage+1),totalCount);
			$(this).siblings("li").removeClass("active");
		}
		else if(showSize>=pageSize){
			$(this).attr("class","active");
			$(this).siblings("li").removeClass("active");
		}
		else if($(this).text()<=Math.ceil(showSize/2)){
			$(this).attr("class","active");
			$(this).siblings("li").removeClass("active");
		}
		else{
			$("#page").empty()
			setPage($(this).text(),totalCount);
			$(this).siblings("li").removeClass("active");
		}
	})
}